from rllab import spaces
from rllab.envs.base import Step
from rllab.envs.base import Env

import generate_data as gd
import train_dynamics as td
from custom_swimmer_env import CustomSwimmerEnv

from abc import ABCMeta, abstractmethod
import traceback
import time
import pickle
import itertools
import numpy as np
import multiprocessing as mp
from multiprocessing.managers import BaseManager

class NetworkWrapper(object):
    '''A simple wrapper over a network object that loads from checkpoint.'''
    def __init__(self, root_prefix, trainparams):
        self._model = trainparams.netcls()
        self._model.initialize(trainparams)
        # load last epoch checkpoint
        suffix = '' # no more saving every epoch '-epoch{}'.format(trainparams.nepoch-1)
        self._model.load_checkpoint(root_prefix, suffix)
    
    @property
    def model(self):
        return self._model

#class NetworkManager(BaseManager):
#    pass
#NetworkManager.register('NetworkWrapper', NetworkWrapper)

class EnsembleType(object):
    '''A callable object that decides how to combine multiple NetworkWrapper's predictions together into one.'''
    __metaclass__ = ABCMeta
    
    def reset(self, dynamics_models):
        # reset all underlying dyanmics models
        for dmodel in dynamics_models:
            dmodel.model.reset()
    
    @abstractmethod
    def step(self, input_single, dynamics_models):
        raise NotImplementedError
    
    @staticmethod
    @abstractmethod
    def name():
        raise NotImplementedError

class EnsembleAverage(EnsembleType):
    def step(self, input_single, dynamics_models):
        next_obs = np.array([dmodel.model.predict_step(input_single) for dmodel in dynamics_models])
        next_obs = np.mean(next_obs, axis=0)
        return next_obs
    
    @staticmethod
    def name():
        return 'AVG'

class EnsembleThompson(EnsembleType):
    def __init__(self):
        self.rd = np.random.random() # for picking one of the models
        
    def step(self, input_single, dynamics_models):
        ix = int(np.floor(self.rd * len(dynamics_models)))
        next_obs = dynamics_models[ix].model.predict_step(input_single)
        return next_obs
    
    @staticmethod
    def name():
        return 'THOMPSON'

class EnsembleRandom(EnsembleType):
        
    def step(self, input_single, dynamics_models):
        ix = int(np.floor(np.random.random() * len(dynamics_models)))
        for i, dmodel in enumerate(dynamics_models):
            # update all models in lockstep
            if i == ix:
                # get observations from the randomly picked models
                next_obs = dmodel.model.predict_step(input_single)
            else:
                dmodel.model.update_history(input_single)
        return next_obs
    
    @staticmethod
    def name():
        return 'RANDOM'

class DynamicsWrapper(Env):
    '''A wrapper around a learned dynamics model to use it as an environment.'''
    
    def __init__(self, root_prefix, senv, trainparams_list, idstr, ensemble_combine_cls):
        self.senv = senv
        self.goal_augmented = senv.goal_augmented
        self.trainparams_list = trainparams_list
        self.root_prefix = root_prefix
        self.idstr = idstr
        # dynamics models are not augmented
        self.dynamics_models = None # lazily load models on first reset
        self.ensemble_combine_cls = ensemble_combine_cls
    
    def _load_models(self):
        # load ensemble of models from checkpoints
        # load using a manager so different instances use separate processes
        self.dynamics_models = []
        for i, trainparams in enumerate(self.trainparams_list):
            #print('Loading model {}'.format(i))
            dmodel = NetworkWrapper(self.root_prefix, trainparams)
            self.dynamics_models.append(dmodel)
    
    def name(self):
        if self.ensemble_size() == 1:
            return 'DynamicsWrapper-{}-{}'.format(self.senv.name(), self.trainparams_list[0].name())
        else:
            return 'DynamicsWrapper-{}-{}-size{}-fn{}'.format(self.senv.name(), self.idstr, self.ensemble_size(), self.ensemble_combine_cls.name())
    
    def reset(self):
        if self.dynamics_models is None:
            # lazily load on first reset
            #print('Loading dynamics models.')
            self._load_models()
            #print('Finished loading dynamics models.')
        
        # make sure the internal current state is without goals
        self.current_state = self.senv.reset()
        if self.goal_augmented:
            self.current_state = self.current_state[len(self.goals):]
        self.currstep = 0
        self.ensemble_fn = self.ensemble_combine_cls() # new instance of the ensemble combination
        self.ensemble_fn.reset(self.dynamics_models)
        
        # make sure the returned obs are properly augmented if needed
        if self.goal_augmented:
            returned_obs = np.concatenate([self.goalvals,self.current_state])
        else:
            returned_obs = self.current_state
        return returned_obs
    
    def ensemble_size(self):
        return len(self.trainparams_list)
    
    @property
    def action_space(self):
        return self.senv.action_space
    
    @property
    def observation_space(self):
        return self.senv.observation_space
    
    def step(self, action):
        # assume has goalvel
        self.currstep += 1
        input_single = np.concatenate((self.current_state, action))
        next_obs = self.ensemble_fn.step(input_single, self.dynamics_models)
        #reward = 1.0 if np.all(np.abs(next_obs[self.goalixs] - self.goalvals) <= self.goalthresholds) else 0.0
        reward = self.senv.compute_reward(next_obs)
        done = False
        if self.senv.single_reward:
            # stop on first reward
            done = reward > 0.99
        # advance the state
        self.current_state = next_obs
        if self.goal_augmented:
            returned_obs = np.concatenate([self.goalvals,next_obs])
        else:
            returned_obs = next_obs
        return Step(returned_obs, reward, done)
 

    # custom pickling
    def __getstate__(self):
        #print('********Pickling\n{}'.format(''.join(traceback.format_stack())))
        # do not save the manager stuff
        d = self.__dict__.copy()
        del d['dynamics_models']
        return d
    
    def __setstate__(self, d):
        #print('********UnPickling\n{}'.format(''.join(traceback.format_stack())))
        self.__dict__ = d
        self.dynamics_models = None # load models on first reset
