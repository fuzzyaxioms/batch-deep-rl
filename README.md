# README #

Batch Model-based Deep RL

### Dependencies ###

This project uses the `rllab` library ([https://github.com/rll/rllab]) and the Mujoco simulator.

### File Overview ###

The main entry point to running experiments is `experiment_script.sh` which runs `run_experiment_daniel.py`.
The script is only there to run multiple experiments in sequence.

`run_experiment_daniel.py` unfortunately has many settings that are hard-coded, which you can see from
some of the commented out code. There are three main modes for experiments: training the ensemble of dynamics
models, training policies on ensemble or single environments, or testing trained policies on environments.
Experiments are usually run in that order as well. This mode is controlled by which code you uncomment and run,
as well as the `--test` flag which indicates you want to test already trained policies.

`run_experiment.py` is deprecated and should be ignored. For other files, follow the chain of dependencies
starting from `run_experiment_daniel.py` to see what they are used for.