'''Train dynamics models of environments.'''

import generate_data as gd

from rllab.envs.mujoco.swimmer_env import SwimmerEnv

from custom_swimmer_env import CustomSwimmerEnv

from abc import ABCMeta, abstractmethod
import tensorflow as tf
import time
import numpy as np
import os
import itertools

checkpointpath = 'checkpoints'

class NetworkParams(object):
    '''Hyperparameters of the function approximator'''
    def __init__(self, seed, nhidden, predictdiff, dropout, batchnorm):
        self.seed = seed
        self.nhidden = nhidden
        self.predictdiff = predictdiff
        self.dropout = dropout
        self.batchnorm = batchnorm
    
    def name(self):
        return 'sd{}-nh{}-pd{}-do{}-bn{}'.format(self.seed, self.nhidden, self.predictdiff, self.dropout, self.batchnorm)

class Network(object):
    '''Trainable function approximation. This is a wrapper that abstracts tf/pytorch/chainer/...'''
    __metaclass__ = ABCMeta
    
    # training from data
    @abstractmethod
    def initialize(self, trainparams):
        raise NotImplementedError
    
    @abstractmethod
    def train_batch(self, inputs, targets):
        raise NotImplementedError
    
    @abstractmethod
    def test_batch(self, inputs, targets):
        raise NotImplementedError
    
    # for using as a dynamics model, works for both MDP and POMDP
    @abstractmethod
    def reset(self):
        raise NotImplementedError
    
    @abstractmethod
    def update_history(self, input_single):
        '''Just update the internal history.'''
        raise NotImplementedError
    
    @abstractmethod
    def predict_step(self, input_single):
        '''Advance the history and return prediction of next state'''
        raise NotImplementedError
    
    # pickling and unpickling
    @abstractmethod
    def save_checkpoint(self, root_prefix, suffix):
        raise NotImplementedError
    
    @abstractmethod
    def load_checkpoint(self, root_prefix, suffix):
        raise NotImplementedError

class TFMLP(Network):
    '''Tensorflow MLP'''
    
    def _build_graph(self):
        self.input_placeholder = tf.placeholder(tf.float32,
                                                shape=[None, self.trainparams.nfeatures])
        self.output_placeholder = tf.placeholder(tf.float32,
                                                shape=[None, self.trainparams.noutputs])
        self.dropout_placeholder = tf.placeholder(tf.float32,
                                                  shape=[])
        self.training_placeholder = tf.placeholder(tf.bool)
        
        with tf.variable_scope('dense1') as scope:
            W1 = tf.get_variable('weights', [self.trainparams.nfeatures, self.netparams.nhidden],
                     initializer=tf.contrib.layers.xavier_initializer())
            b1 = tf.get_variable('biases', [self.netparams.nhidden],
                   initializer=tf.constant_initializer(0.0))
            hidden1 = tf.add(tf.matmul(self.input_placeholder, W1), b1)
            relu1 = tf.nn.relu(hidden1)

        with tf.variable_scope('dense2') as scope:
            W2 = tf.get_variable('weights', [self.netparams.nhidden, self.netparams.nhidden],
                     initializer=tf.contrib.layers.xavier_initializer())
            b2 = tf.get_variable('biases', [self.netparams.nhidden],
                   initializer=tf.constant_initializer(0.0))
            if self.trainparams.netparams.batchnorm:
                relu1 = tf.layers.batch_normalization(relu1, training=self.training_placeholder)
            hidden2 = tf.add(tf.matmul(relu1, W2), b2)
            relu2 = tf.nn.relu(hidden2)
            if self.trainparams.netparams.batchnorm:
                relu2 = tf.layers.batch_normalization(relu2, training=self.training_placeholder)
            drop2 = tf.nn.dropout(relu2, keep_prob=self.dropout_placeholder)

        with tf.variable_scope('dense3') as scope:
            W3 = tf.get_variable('weights', [self.netparams.nhidden, self.trainparams.noutputs],
                     initializer=tf.contrib.layers.xavier_initializer())
            b3 = tf.get_variable('biases', [self.trainparams.noutputs],
                   initializer=tf.constant_initializer(0.0))
            pred = tf.add(tf.matmul(drop2, W3), b3)
            if not self.netparams.predictdiff:
                self.pred = pred
            else:
                skip_conn_input = tf.slice(self.input_placeholder, [0,0], [-1, self.trainparams.noutputs])
                self.pred = tf.add(skip_conn_input, pred)
        
        self.pred_vector = tf.reshape(self.pred, [-1])
        self.output_placeholder_vector = tf.reshape(self.output_placeholder, [-1])
        self.loss = tf.reduce_mean(tf.square(self.pred_vector - self.output_placeholder_vector))
        
        # for batch norm updates
        extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(extra_update_ops):
            self.optimizer = tf.train.AdamOptimizer(learning_rate=self.lr).minimize(self.loss)
    
    def initialize(self, trainparams):
        self.trainparams = trainparams
        self.lr = trainparams.lr
        self.netparams = trainparams.netparams
        self.tfgraph = tf.Graph()
        # limit to single threads to reduce overhead
        self.sess = tf.Session(graph=self.tfgraph, config=tf.ConfigProto(intra_op_parallelism_threads=1,inter_op_parallelism_threads=1))

        with self.tfgraph.as_default():
            # initialize the seeds
            np.random.seed(self.netparams.seed)
            tf.set_random_seed(self.netparams.seed)
            
            self._build_graph()
            self.sess.run(tf.global_variables_initializer())
            self.saver = tf.train.Saver(max_to_keep=None)

    def train_batch(self, inputs, targets):
        with self.tfgraph.as_default():
            feed = {
                self.input_placeholder: inputs,
                self.output_placeholder: targets,
                self.dropout_placeholder: 0.5 if self.netparams.dropout else 1.0,
                self.training_placeholder: True
            }
            loss, _ = self.sess.run([self.loss, self.optimizer], feed_dict=feed)
        return loss

    def test_batch(self, inputs, targets):
        with self.tfgraph.as_default():
            feed = {
                self.input_placeholder: inputs,
                self.output_placeholder: targets,
                self.dropout_placeholder: 1.0,
                self.training_placeholder: False
            }
            loss, = self.sess.run([self.loss], feed_dict=feed)
        return loss
    
    def reset(self):
        # this is an MDP so no need to reset anything
        pass
    
    def update_history(self, input_single):
        # this is an MDP so no need to update history
        pass
    
    def predict_step(self, input_single):
        # return prediction
        with self.tfgraph.as_default():
            feed = {
                self.input_placeholder: input_single[np.newaxis,:],
                self.dropout_placeholder: 1.0,
                self.training_placeholder: False
            }
            pred, = self.sess.run([self.pred], feed_dict=feed)
        return pred.reshape(-1)

    def save_checkpoint(self, root_prefix, suffix):
        global checkpointpath
        with self.tfgraph.as_default():
            self.saver.save(self.sess, os.path.join(root_prefix, checkpointpath, self.trainparams.name()+suffix))
    
    def load_checkpoint(self, root_prefix, suffix):
        # initialize first, then load
        global checkpointpath
        with self.tfgraph.as_default():
            self.saver.restore(self.sess, os.path.join(root_prefix, checkpointpath, self.trainparams.name()+suffix))

class TrainParams(object):
    '''All parameters of a trained network.'''
    def __init__(self, dataparams, netparams, netcls, nepoch, valfrac, lr, batchsize):
        self.dataparams = dataparams
        self.netparams = netparams
        self.netcls = netcls
        self.nepoch = nepoch
        self.valfrac = valfrac
        self.lr = lr
        self.batchsize = batchsize
        self.nfeatures = int(dataparams.env.observation_space.flat_dim + dataparams.env.action_space.flat_dim)
        self.noutputs = int(dataparams.env.observation_space.flat_dim)
    
    def addendum(self, inputs, targets):
        # use inputs and targets purely for statistics
        self.ndata = inputs.shape[0]
        self.nbatch = int(np.ceil(self.ndata / self.batchsize))
    
    def name(self):
        return 'dpL{}J-npL{}J-ne{}-vf{}-lr{}'.format(
            self.dataparams.name(), self.netparams.name(), self.nepoch, self.valfrac, self.lr
        )

def train_network(root_prefix, trainparams, inputs, targets):
    global checkpointpath
    trainparams.addendum(inputs, targets)
    gd.validate_path(os.path.join(root_prefix, checkpointpath))
    net = trainparams.netcls()
    # this is where the seed of the tf graph and numpy are set
    net.initialize(trainparams)
    
    # train for epochs and checkpoint every epoch
    valcut = int(trainparams.ndata*(1 - trainparams.valfrac))
    indices = np.random.permutation(trainparams.ndata)
    train_indices = indices[:valcut]
    val_indices = indices[valcut:]
    tloss = np.zeros(trainparams.nepoch)
    vloss = np.zeros(trainparams.nepoch)
    print('==== Training {} ===='.format(trainparams.name()))
    for epoch in range(trainparams.nepoch):
        starttime = time.time()
        print('Epoch {}'.format(epoch))
        for b in range(trainparams.nbatch):
            # create the minibatch
            batchindices = np.random.choice(train_indices, size=trainparams.batchsize, replace=False)
            batch_inputs = np.copy(inputs[batchindices,:])
            batch_targets = np.copy(targets[batchindices,:])
            
            # train batch
            curr_loss = net.train_batch(batch_inputs, batch_targets)
            tloss[epoch] += curr_loss
        tloss[epoch] /= trainparams.nbatch
        vloss[epoch] = net.test_batch(inputs[val_indices,:], targets[val_indices,:])
        print('  tloss {:.8f} vloss {:.8f} elapsed {:.3f}s'.format(tloss[epoch], vloss[epoch], time.time()-starttime))
        # save only the final epoch's model
        if epoch == trainparams.nepoch-1:
            net.save_checkpoint(root_prefix, '')
    np.savetxt(os.path.join(root_prefix, checkpointpath, trainparams.name()+'.csv'), np.array([tloss, vloss]), delimiter = ',')

def convert_data(data):
    ndata = len(data)
    inputs_list = []
    targets_list = []
    for i in range(ndata):
        obs, acts, _ = data[i]
        for t in range(len(acts)):
            inputs_list.append(np.concatenate([obs[t], acts[t]]))
            targets_list.append(obs[t+1])
    return np.array(inputs_list), np.array(targets_list)

        
