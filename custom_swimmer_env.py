from rllab import spaces
from rllab.envs.base import Step
from rllab.envs.base import Env
from rllab.envs.mujoco.swimmer_env import SwimmerEnv
from rllab.core.serializable import Serializable
from rllab.misc import logger
from rllab.misc import autoargs

import numpy as np

class CustomSwimmerEnv(Env):

    def __init__(self, goals, single_reward, goal_augmented):
        '''
        goals is [(ob ix, goal val, threshold)...]
        single_reward: True if we only stop when we first reach reward
        goal_augmented: whether the observation space is augmented with the goal (augmented at the beginning)
        '''
        self.senv = SwimmerEnv(action_noise=0.0)
        assert(len(goals) > 0)
        self.goals = goals
        self.goalvals = np.array([gv for _,gv,_ in self.goals])
        self.goalthresholds = np.array([th for _,_,th in self.goals])
        self.goalixs = np.array([ix for ix,_,_ in self.goals])
        self.single_reward = single_reward
        self.goal_augmented = goal_augmented
        self.init_state = np.ones(18) * 0.05
        self.is_pomdp = False
    
    def name(self):
        '''Unique str name of the parameters of this env.'''
        goalstr = 'L' + ('_'.join('{}-{}-{}'.format(ix, gv, th) for (ix, gv, th) in self.goals)) + 'J'
        return 'CustomSwimmer-go{}-sr{}-ga{}'.format(goalstr, self.single_reward, self.goal_augmented)
    
    def reset(self):
        self.senv.reset(init_state=self.init_state)
        self.currstep = 0
        return self._get_current_obs()
       
    @property
    def action_space(self):
        return self.senv.action_space
    
    @property
    def observation_space(self):
        # copied from mujoco_env.py
        BIG = 1e6
        shp = self._get_current_obs().shape
        ub = BIG * np.ones(shp)
        return spaces.Box(ub * -1, ub)
    
    def _get_current_obs(self):
        curr_obs = np.concatenate([
            self.senv.model.data.qpos.flat,
            self.senv.model.data.qvel.flat,
            self.senv.get_body_com("torso").flat,
            self.senv.get_body_comvel("torso")[0].flat,
        ]).reshape(-1)
        if self.goal_augmented:
            curr_obs = np.concatenate([self.goalvals, curr_obs])
        return curr_obs

    def compute_reward(self, obs):
        '''Given observation after transition, compute the known reward.'''
        return 1.0 if np.all(np.abs(obs[self.goalixs] - self.goalvals) <= self.goalthresholds) else 0.0
    
    def step(self, action):
        self.currstep += 1
        self.senv.forward_dynamics(action)
        next_obs = self._get_current_obs()
        done = False
        reward = 1.0 if np.all(np.abs(next_obs[self.goalixs] - self.goalvals) <= self.goalthresholds) else 0.0
        if self.single_reward:
            done = reward > 0.99
        return Step(next_obs, reward, done)

class CustomSwimmerDenseGoalEnv(Env):

    def __init__(self, goals, time_augmented):
        '''
        goals is [(ob ix, goal val)...]
        no threshold, use dense rewards which are negative L1 distance
        doesn't terminate on first reward obviously
        time_augmented indicates whether the observations include the current timestep
        basically to let the q-function be a proper q-function and the end of epsidoes be proper terminal states
        '''
        self.senv = SwimmerEnv(action_noise=0.0)
        assert(len(goals) > 0)
        self.goals = goals
        self.goalvals = np.array([gv for _,gv in self.goals])
        self.goalixs = np.array([ix for ix,_ in self.goals])
        self.goalthresholds = None
        self.single_reward = False
        self.goal_augmented = False
        self.time_augmented = time_augmented
        self.init_state = np.ones(18) * 0.05
    
    def name(self):
        '''Unique str name of the parameters of this env.'''
        goalstr = 'L' + ('_'.join('{}-{}'.format(ix, gv) for (ix, gv) in self.goals)) + 'J'
        return 'CustomSwimmerDenseGoal-go{}-ta{}'.format(goalstr, self.time_augmented)
    
    def reset(self):
        self.senv.reset(init_state=self.init_state)
        self.currstep = 0
        return self._get_current_obs()
       
    @property
    def action_space(self):
        return self.senv.action_space
    
    @property
    def observation_space(self):
        # copied from mujoco_env.py
        BIG = 1e6
        shp = self._get_current_obs().shape
        ub = BIG * np.ones(shp)
        return spaces.Box(ub * -1, ub)
    
    def _get_current_obs(self):
        curr_obs = np.concatenate([
            self.senv.model.data.qpos.flat,
            self.senv.model.data.qvel.flat,
            self.senv.get_body_com("torso").flat,
            self.senv.get_body_comvel("torso")[0].flat,
        ]).reshape(-1)
        if self.time_augmented:
            # add the timestep, but scaled down to approximately match the magnitude of the other values
            curr_obs = np.concatenate([[self.currstep / 100.0], curr_obs])
        return curr_obs

    def step(self, action):
        self.currstep += 1
        self.senv.forward_dynamics(action)
        next_obs = self._get_current_obs()
        done = False
        if not self.time_augmented:
            next_obs_clean = next_obs
        else:
            next_obs_clean = next_obs[1:]
        reward = -np.sum(np.abs(next_obs_clean[self.goalixs] - self.goalvals))
        return Step(next_obs, reward, done)

class CustomSwimmerDenseEnv(Env):

    def __init__(self, rewardscales, time_augmented):
        '''
        reward scales is [(ix, scale)] for computing the reward function
        time_augmented indicates whether the observations include the current timestep
        basically to let the q-function be a proper q-function and the end of epsidoes be proper terminal states
        '''
        self.senv = SwimmerEnv(action_noise=0.0)
        self.rewardscales = rewardscales
        self.rewardixs = np.array([ix for ix,_ in rewardscales])
        self.rewardscs = np.array([sc for _,sc in rewardscales])
        self.time_augmented = time_augmented
        self.init_state = np.ones(18) * 0.05
        self.currstep = 0
    
    def name(self):
        '''Unique str name of the parameters of this env.'''
        goalstr = 'L' + ('_'.join('{}-{}'.format(ix, sc) for (ix, sc) in self.rewardscales)) + 'J'
        return 'CustomSwimmerDense-rs{}-ta{}'.format(goalstr, self.time_augmented)
    
    def reset(self):
        self.senv.reset(init_state=self.init_state)
        self.currstep = 0
        return self._get_current_obs()
       
    @property
    def action_space(self):
        return self.senv.action_space
    
    @property
    def observation_space(self):
        # copied from mujoco_env.py
        BIG = 1e6
        shp = self._get_current_obs().shape
        ub = BIG * np.ones(shp)
        return spaces.Box(ub * -1, ub)
    
    def _get_current_obs(self):
        curr_obs = np.concatenate([
            self.senv.model.data.qpos.flat,
            self.senv.model.data.qvel.flat,
            self.senv.get_body_com("torso").flat,
            self.senv.get_body_comvel("torso")[0].flat,
        ]).reshape(-1)
        if self.time_augmented:
            # add the timestep, but scaled down to approximately match the magnitude of the other values
            curr_obs = np.concatenate([[self.currstep / 100.0], curr_obs])
        return curr_obs

    def step(self, action):
        self.currstep += 1
        self.senv.forward_dynamics(action)
        next_obs = self._get_current_obs()
        done = False
        if not self.time_augmented:
            next_obs_clean = next_obs
        else:
            next_obs_clean = next_obs[1:]
        reward = np.sum(next_obs_clean[self.rewardixs] * self.rewardscs)
        return Step(next_obs, reward, done)
