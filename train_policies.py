from rllab.algos.trpo import TRPO as TRPO_th
from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy as GaussianMLPPolicy_th
import lasagne.nonlinearities as NL

from rllab.baselines.linear_feature_baseline import LinearFeatureBaseline
from rllab.envs.normalized_env import normalize
from rllab.misc import logger
from rllab.misc.ext import set_seed
from rllab.sampler import parallel_sampler

#from sandbox.rocky.tf.algos.trpo import TRPO as TRPO_tf 
#from sandbox.rocky.tf.policies.gaussian_mlp_policy import GaussianMLPPolicy
#from minimal_gauss_mlp_policy import GaussianMLPPolicy # MAML policy
#from sandbox.rocky.tf.envs.base import TfEnv

import generate_data as gd
import tensorflow as tf

import time
import pickle
import itertools
import os
import numpy as np

snapshotpath = 'policysnapshots'

class TrainPolicyParams(object):
    def __init__(self, env, horizon, num_iter, batchsize, act_fn, nhidden, seed, nparallel):
        self.env = env
        self.horizon = horizon
        self.num_iter = num_iter
        self.batchsize = batchsize
        self.act_fn = act_fn # string name of relu or tanh
        self.nhidden = nhidden
        assert(self.act_fn == 'relu' or self.act_fn == 'tanh')
        self.seed = seed
        self.nparallel = nparallel
    
    def name(self):
        return '{}-testhorizon{}-pbs{}-af{}-nh{}-psd{}-np{}'.format(self.env.name(), self.horizon, self.batchsize, self.act_fn, self.nhidden, self.seed, self.nparallel)

def get_policy_path(root_prefix, trainpolparams):
    global snapshotpath
    curpath = os.path.join(root_prefix, snapshotpath, trainpolparams.name())
    gd.validate_path(curpath)
    return curpath
'''
def train_policy(root_prefix, trainpolparams):
    # DEPRECATED, tensorflow seemed to lead to problems, and theano seems faster
    # note that the learned policy assumes a normalized environment
    tfgraph = tf.Graph()
    #sess = tf.Session(graph=tfgraph)
    
    with tfgraph.as_default():
        set_seed(trainpolparams.seed)
    curpath = get_policy_path(root_prefix, trainpolparams)
    logger.set_snapshot_dir(curpath)
    logger.set_snapshot_gap(50)
    logger.set_snapshot_mode('gap')
    with tfgraph.as_default():
        parallel_sampler.initialize(n_parallel=trainpolparams.nparallel)
        parallel_sampler.set_seed(trainpolparams.seed)

    env = TfEnv(normalize(trainpolparams.env))
    env.reset() # loads dynamic wrapper if hasn't been loaded before
    
    with tfgraph.as_default():
        policy = GaussianMLPPolicy(
            name="policy",
            env_spec=env.spec,
            hidden_nonlinearity=tf.nn.relu,
            hidden_sizes=(32, 32)
        )

    baseline = LinearFeatureBaseline(env_spec=env.spec)
    
    with tfgraph.as_default():
        algo = TRPO_tf(
            env=env,
            policy=policy,
            baseline=baseline,
            batch_size=trainpolparams.horizon*trainpolparams.batchsize,
            max_path_length=trainpolparams.horizon,
            n_itr=trainpolparams.num_iter,
            step_size=0.1,
            force_batch_sampler=True, # don't use vectorized sampler
            # Uncomment both lines (this and the plot parameter below) to enable plotting
            # plot=True,
        )
        algo.train()
    # finished with this session for training policies
    #sess.close()
'''

def train_policy_theano(root_prefix, trainpolparams):
    # use the theano versions
    # note that the learned policy assumes a normalized environment
    
    set_seed(trainpolparams.seed)
    curpath = get_policy_path(root_prefix, trainpolparams)
    trainstatspath = os.path.join(curpath, 'train_stats.csv') # training stats table
    logger.set_snapshot_dir(curpath)
    logger.set_snapshot_gap(50)
    logger.set_snapshot_mode('gap')
    parallel_sampler.initialize(n_parallel=trainpolparams.nparallel)
    parallel_sampler.set_seed(trainpolparams.seed)

    env = normalize(trainpolparams.env)
    env.reset() # loads dynamic wrapper if hasn't been loaded before
    if trainpolparams.act_fn == 'tanh':
        policy_act_fn = NL.tanh
    elif trainpolparams.act_fn == 'relu':
        policy_act_fn = NL.rectify
    policy = GaussianMLPPolicy_th(
        env_spec=env.spec,
        hidden_nonlinearity=policy_act_fn,
        hidden_sizes=(trainpolparams.nhidden, trainpolparams.nhidden)
    )

    baseline = LinearFeatureBaseline(env_spec=env.spec)
    
    algo = TRPO_th(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=trainpolparams.horizon*trainpolparams.batchsize,
        max_path_length=trainpolparams.horizon,
        n_itr=trainpolparams.num_iter,
        discount=0.99,
        step_size=0.1, # 0.01 seems to be the default in the examples, but 0.1 seems faster for us
        #force_batch_sampler=True, # don't use vectorized sampler
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    # save the training information
    logger.add_tabular_output(trainstatspath)
    algo.train()
    logger.remove_tabular_output(trainstatspath)