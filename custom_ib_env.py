from rllab import spaces
from rllab.envs.base import Step
from rllab.envs.base import Env
from rllab.envs.gym_env import convert_gym_space
from rllab.core.serializable import Serializable
from rllab.misc import logger
from rllab.misc import autoargs
from industrialbenchmark.industrialbenchmark.industrial_benchmark_openai.OpenAI_IB import OpenAI_IB

import numpy as np

class CustomIBEnv(Env):

    def __init__(self):
        '''
        Custom wrapper around the IB simulator.
        '''
        self.markov = False
        self.senv = OpenAI_IB(markov_state=self.markov) # use mostly defaults
        self.single_reward = False
        self.goal_augmented = False
        self.is_pomdp = not self.markov
    
    def name(self):
        '''Unique str name of the parameters of this env.'''
        return 'CustomIB-ma{}'.format(self.markov)
    
    def reset(self):
        self.senv.reset()
        self.currstep = 0
        return self._get_current_obs()
       
    @property
    def action_space(self):
        return convert_gym_space(self.senv.action_space)
    
    @property
    def observation_space(self):
        return convert_gym_space(self.senv.observation_space)
    
    def compute_reward(self, obs):
        '''Given observation after transition, compute the known reward.'''
        return (-obs[-1] - 3*obs[-2]) / 100.0
    
    def _get_current_obs(self):
        return self.senv.observation

    def step(self, action):
        self.currstep += 1
        next_obs, next_r, _, _ = self.senv.step(action)
        done = False
        return Step(next_obs, next_r, done)
    
    def __getstate__(self):
        d = self.__dict__.copy()
        del d['senv']
        return d
    
    def __setstate__(self, state):
        self.__dict__ = state
        self.senv = OpenAI_IB(markov_state=self.markov)

