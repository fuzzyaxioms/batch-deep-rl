import generate_data as gd
import train_dynamics as td
import dynamics_wrapper as dw
import train_policies as tp
import evaluate as ev
import online_policy as op

from rllab.envs.normalized_env import normalize
from sandbox.rocky.tf.envs.base import TfEnv
from custom_swimmer_env import CustomSwimmerEnv
import tensorflow as tf

import time
import pickle
import itertools
import joblib
import os
import multiprocessing as mp
import numpy as np
import collections

model_num = 50
policy_num = 5
def data_stats(d):
    vels = []
    for obs, acts, rs in d:
        for ob in obs:
            vels.append(ob[-1])
    vels = np.array(vels)
    sorted_vels = np.sort(vels)
    print(sorted_vels)

def do_training_dynamics(root_prefix, trainparams_list, inputs, targets, nprocesses):
    # use the same data (inputs, targets) to train all models
    with mp.Pool(processes=nprocesses) as pool:
        results = []
        for trainparams in trainparams_list:
            res = pool.apply_async(td.train_network, (root_prefix, trainparams, inputs, targets))
            results.append(res)
        for res in results:
            res.get()

def do_training_policies(root_prefix, trainpolparams_list, nprocesses):
    if nprocesses > 1:
        with mp.Pool(processes=nprocesses) as pool:
            results = []
            for trainpolparams in trainpolparams_list:
                res = pool.apply_async(tp.train_policy, (root_prefix, trainpolparams))
                results.append(res)
            for res in results:
                res.get()
    else:
        for trainpolparams in trainpolparams_list:
            tp.train_policy(root_prefix, trainpolparams)

def do_evaluate(root_prefix, senv_test, trainpolparams_list, nprocesses):
    results = dict()
    tasks = []
    with mp.Pool(processes=nprocesses) as pool:
        for trainpolparams in trainpolparams_list:
            num_iter = trainpolparams.num_iter
            for i in range(0, num_iter, 50):
                rewards_oracle_async = pool.apply_async(ev.test_policy, (root_prefix, trainpolparams, senv_test, i, 100))
                rewards_self_async = pool.apply_async(ev.test_policy, (root_prefix, trainpolparams, trainpolparams.env, i, 100))
                tasks.append((trainpolparams, i, rewards_self_async, rewards_oracle_async))
        for trainpolparams, i, rewards_self_async, rewards_oracle_async in tasks:
            rewards_self = rewards_self_async.get()
            rewards_oracle = rewards_oracle_async.get()
            env_name = trainpolparams.env.name()
            pol_seed = trainpolparams.seed
            results.setdefault(env_name, dict()).setdefault(pol_seed, list()).append((i, np.mean(rewards_self), np.mean(rewards_oracle)))
            
    # dump results to generic results file
    with open(os.path.join(root_prefix, 'results.pkl'),'wb') as f:
#        print(results)
        pickle.dump(results, f)
    for i in results:
        print(i)
        for j in results[i]:
            print(j)
            for k in results[i][j]:
                print(k)

def do_online_evaluate(root_prefix, senv_test, trainpolparams_seed_dict, nprocesses):
    for repeat_last in [0.8, 0.5, 0.2, 0]: 
        for error_decay in [1, 0.8, 0.5, 0.2, 0]:
            do_online_evaluate_helper(root_prefix, senv_test, trainpolparams_seed_dict, nprocesses, error_decay, repeat_last)

def do_online_evaluate_helper(root_prefix, senv_test, trainpolparams_seed_dict, nprocesses, error_decay, repeat_last):
    results = dict()
    tasks = []
    ntrials = 100
    freq_reward_history = dict()
    with mp.Pool(processes=nprocesses) as pool:
        for sd in trainpolparams_seed_dict:
            trainpolparams_list = trainpolparams_seed_dict[sd]
            num_iter = trainpolparams_list[0].num_iter
            for i in range(50, num_iter, 50):
                rewards_oracle_async = pool.apply_async(op.test_policy, (root_prefix, trainpolparams_list, senv_test, i, ntrials, error_decay, repeat_last))
                rewards_self_async = pool.apply_async(op.test_policy, (root_prefix, trainpolparams_list, trainpolparams_list[0].env, i, ntrials, error_decay, repeat_last))
                tasks.append((sd, i, rewards_self_async, rewards_oracle_async))
        for sd, i, rewards_self_async, rewards_oracle_async in tasks:
            rewards_self, common_freqs_self = rewards_self_async.get()
            rewards_oracle, common_freqs_oracle = rewards_oracle_async.get()
            results.setdefault(sd, list()).append((i, np.mean(rewards_self), np.mean(rewards_oracle)))
            for (f, r) in zip(common_freqs_oracle, rewards_oracle):
                freq_reward_history.setdefault(f, list()).append(r)
    with open(os.path.join(root_prefix, 'results', 'online_results_mn{}_ed{}_rl{}.pkl'.format(model_num, error_decay, repeat_last)),'wb') as f:
        pickle.dump(results, f)
    print("mn{}_ed{}_rl{}".format(model_num, error_decay, repeat_last))
    with open(os.path.join(root_prefix, 'results', 'online_freq_results_mn{}_ed{}_rl{}.pkl'.format(model_num, error_decay, repeat_last)),'wb') as f:
        pickle.dump(freq_reward_history, f)
#    for sd in results:
#        print(sd)
#        for i in results[sd]:
#            print(i)



def check_mse(root_prefix, env, trainpolparams, trainparams_list, ntrials):
    '''Runs the trained policy in given env for multiple rollouts and computes MSE stats for the given list of models.'''
    DEBUG = True
    learned_envs = [dw.DynamicsWrapper(root_prefix, env, [trainparams], 'foo', dw.EnsembleAverage) for trainparams in trainparams_list]
    tfgraph = tf.Graph()
    with tf.Session(graph=tfgraph) as sess:
        env = TfEnv(normalize(env))
        policy_path = tp.get_policy_path(root_prefix, trainpolparams)
        pol = joblib.load(os.path.join(policy_path, 'itr_{}.pkl'.format(int(trainpolparams.num_iter / 50) * 50)))['policy']
        acc_mse = np.zeros((len(trainparams_list),))
        last_acc_mse = np.zeros((len(trainparams_list),))
        for i in range(ntrials):
            ob = env.reset()
            pol.reset()
            for lenv in learned_envs:
                lenv.reset()
            if DEBUG:
                print('trial {}:'.format(i))
            for t in range(trainpolparams.horizon):
                # get expert action
                a = pol.get_action(ob)[0]
                # get expert observation
                ob, r, done, _ = env.step(a)
                # compute the multistep MSE for the learned envs
                for j in range(len(trainparams_list)):
                    this_ob, _, _, _ = learned_envs[j].step(a)
                    acc_mse[j] += np.sum(np.square(this_ob - ob))
                    last_acc_mse[j] += np.sum(np.square(this_ob[-1] - ob[-1]))
                # ignore done since we only care about prediction MSE
        # average MSE per step
        acc_mse /= ntrials * trainpolparams.horizon
        last_acc_mse /= ntrials * trainpolparams.horizon
        # print out the results
        results = dict()
        for j in range(len(trainparams_list)):
            results[trainparams_list[j].name()] = (acc_mse[j], last_acc_mse[j])
        for k in sorted(results.keys()):
            print('CHECKING MSE: {} = {:.8f} {:.8f}'.format(k, results[k][0], results[k][1]))
    

def do_swimmer_stuff():
    # parameters
    root_prefix = '.'
    goalvel = 0.10
    horizon = 20
    ntraj = 10000
    
    model_seeds = [331491256, 317996, 21791086, 33835675, 6585405,
    85003283, 19922827, 28503138, 75720855, 88341058,
    92657409, 303230, 57584953, 41201072, 28386107,
    17540358, 85586490, 145616729, 152015139, 608403188,
    581981390, 34265261, 324431369, 121177333, 491687403,
    912849316, 184462122, 382923657, 93422015, 383892549,
    374270940, 974900428, 298231920, 939426454, 995461417,
    913453782, 921478568, 446164064, 388896523, 329545745,
    776484502, 334850451, 584821320, 124683310, 623418380,
    994249451, 293598528, 766160162, 830274089, 675407091]
    nhidden = 500
    predictdiff = True
    dropout = False
    nepoch = 10
    valfrac = 0.1
    lr = 0.001
    batchsize = 128
    
    test_horizon = 30
    single_reward = True

    policy_seeds = [331491256, 317996, 21791086, 33835675, 6585405,
    85003283, 19922827, 28503138, 75720855, 88341058,
    92657409, 303230, 57584953, 41201072, 28386107,
    17540358, 85586490, 145616729, 152015139, 608403188,
    581981390, 34265261, 324431369, 121177333, 491687403,
    912849316, 184462122, 382923657, 93422015, 383892549,
    374270940, 974900428, 298231920, 939426454, 995461417,
    913453782, 921478568, 446164064, 388896523, 329545745,
    776484502, 334850451, 584821320, 124683310, 623418380,
    994249451, 293598528, 766160162, 830274089, 675407091]
    # currently the batch sampler doesn't work properly with parallel sampling, leading to some kind of race condition
    # maybe it'll work if we move to pytorch from tensorflow
    #policy_nparallel = 1
    trpo_num_iter = 301

    senv = CustomSwimmerEnv(goalvel, single_reward, horizon)
    senv_test = CustomSwimmerEnv(goalvel, single_reward, test_horizon)
    dparams = gd.DataParams(senv, horizon, ntraj)
    
    '''
    [
    331491256, 317996, 21791086, 33835675, 6585405,
    85003283, 19922827, 28503138, 75720855, 88341058,
    92657409, 303230, 57584953, 41201072, 28386107,
    17540358, 85586490, 145616729, 152015139, 608403188,
    581981390, 34265261, 324431369, 121177333, 491687403,
    912849316, 184462122, 382923657, 93422015, 383892549,
    374270940, 974900428, 298231920, 939426454, 995461417,
    913453782, 921478568, 446164064, 388896523, 329545745,
    776484502, 334850451, 584821320, 124683310, 623418380,
    994249451, 293598528, 766160162, 830274089, 675407091,
    
    250562518, 346167074, 441516361, 64970807, 670018528,
    369843816, 154221372, 802012560, 510710029, 566297351,
    724223574, 600543360, 765480830, 482457786, 234109023,
    626804401, 394806814, 803522011, 96793077, 104848,
    691574437, 706774301, 711715375, 58401816, 236444548,
    945035728, 938293214, 895182543, 407839105, 362501316,
    629333743, 852024366, 595135232, 265174394, 143756579,
    888651646, 854442427, 182852364, 490773228, 273766330,
    22221848, 653190812, 661114719, 892456771, 779845188,
    289729862, 609669704, 396332902, 363995956, 118869516,
    ]
    '''
    ###############################################################################
    ######## generate data
    #gd.generate_data(root_prefix, dparams)
    
    ######## load data
    trajs = gd.load_data(root_prefix, dparams)
    data_stats(trajs)
    inputs, targets = td.convert_data(trajs)
    
    
    ###############################################################################
    ######## train a bunch of models
    trainparams_list = []
    for sd in model_seeds[:model_num]:
        netparams = td.NetworkParams(sd, nhidden, predictdiff, dropout)
        trainparams = td.TrainParams(dparams, netparams, td.TFMLP, inputs, targets, nepoch, valfrac, lr, batchsize)
        trainparams_list.append(trainparams)
    #do_training_dynamics(root_prefix, trainparams_list, inputs, targets, nprocesses=10)
    
    
    ###############################################################################
    ######## train a bunch of policies per model
    trainpolparams_list = []
    trainpolparams_seed_dict = collections.defaultdict(list)
    for trainparams in trainparams_list:
        # load model from checkpoints as an environment
        learned_env = dw.DynamicsWrapper(root_prefix, senv_test, [trainparams], 'foo', dw.EnsembleAverage)
        
        for sd in policy_seeds[:policy_num]:
            trainpolparams = tp.TrainPolicyParams(learned_env, test_horizon, trpo_num_iter, sd, 1)
            trainpolparams_list.append(trainpolparams)
            trainpolparams_seed_dict[sd].append(trainpolparams)
    #do_training_policies(root_prefix, trainpolparams_list, nprocesses=2)
    
    ######## evaluate the policy in an environment
    # run this separately from training models
    #do_evaluate(root_prefix, senv_test, trainpolparams_list, nprocesses=20)
    do_online_evaluate(root_prefix, senv_test, trainpolparams_seed_dict, nprocesses=10) 
    
    ###############################################################################
    ######## train policies on ensemble models
    ensemble_trainpolparams_list = []
    # load model from checkpoints as an environment
    learned_env = dw.DynamicsWrapper(root_prefix, senv_test, trainparams_list, 'group', dw.EnsembleThompson)
    for sd in policy_seeds:
        trainpolparams = tp.TrainPolicyParams(learned_env, test_horizon, trpo_num_iter, sd, 1)
        ensemble_trainpolparams_list.append(trainpolparams)
    #do_training_policies(root_prefix, ensemble_trainpolparams_list, nprocesses=10)
    
    ######## evaluate the policy in an environment
    # run this separately from training models
    #do_evaluate(root_prefix, senv_test, ensemble_trainpolparams_list, nprocesses=10)
    
    
    ###############################################################################
    ######## train policies on oracle env
    oracle_trainpolparams_list = []
    for sd in [331491256]:
        trainpolparams = tp.TrainPolicyParams(senv_test, test_horizon, 101, sd, 20)
        oracle_trainpolparams_list.append(trainpolparams)
    #do_training_policies(root_prefix, oracle_trainpolparams_list, nprocesses=1)
    
    ######## evaluate oracle policies
    #do_evaluate(root_prefix, senv_test, oracle_trainpolparams_list, nprocesses=10)
    
    ######## check MSE of learned models on expert policy
    trainpolparams = tp.TrainPolicyParams(senv_test, test_horizon, 101, 331491256, 20)
    #check_mse(root_prefix, senv_test, trainpolparams, trainparams_list, 100)


def main():
    starttime = time.time()
    do_swimmer_stuff()
    print('Elapsed {:.3f}s'.format(time.time() - starttime))


if __name__ == '__main__':
    main()
