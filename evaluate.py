'''Evaluate policies in environments.'''
from sandbox.rocky.tf.policies.gaussian_mlp_policy import GaussianMLPPolicy
from sandbox.rocky.tf.envs.base import TfEnv
from rllab.envs.normalized_env import normalize

import train_policies as tp

import pickle
import os
import joblib

import numpy as np
import tensorflow as tf

def np2str(arr):
    return ' '.join('{:.4f}'.format(x) for x in arr)

def test_policy(root_prefix, trainpolparams, env, discount, policy_itr, ntrials):
    '''
    Given a Policy (rllab policy), and environment, test the policy and return stats.
    '''
    DEBUG = False
    
    env = normalize(env)
    policy_path = tp.get_policy_path(root_prefix, trainpolparams)
    full_policy_path = os.path.join(policy_path, 'itr_{}.pkl'.format(policy_itr))
    print('FULL POLICY PATH: {}'.format(full_policy_path))
    pol = joblib.load(full_policy_path)['policy']
    rewards = np.zeros((ntrials,)) # only care about total sums of rewards
    
    for i in range(ntrials):
        ob = env.reset()
        pol.reset()
        dfac = 1.0
        for t in range(trainpolparams.horizon):
            a = pol.get_action(ob)[0]
            ob, r, done, _ = env.step(a)
            rewards[i] += dfac*r
            dfac *= discount
            if done:
                break

    return rewards