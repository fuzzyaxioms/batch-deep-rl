import generate_data as gd
import train_dynamics as td
import train_dynamics_rnn as tdrnn
import dynamics_wrapper as dw
import train_policies as tp
import train_hindsight as thd
import evaluate as ev
import online_policy as op

from rllab.envs.normalized_env import normalize
from sandbox.rocky.tf.envs.base import TfEnv
from custom_swimmer_env import CustomSwimmerEnv, CustomSwimmerDenseEnv
from custom_half_cheetah_env import CustomHalfCheetahEnv, CustomHalfCheetahDenseEnv
from custom_ib_env import CustomIBEnv
import tensorflow as tf

import io
import time
import pickle
import itertools
import joblib
import os
import multiprocessing as mp
import numpy as np
import collections
import pyprind
import argparse

def data_stats(d):
    vels = []
    for obs, acts, rs in d:
        if np.random.random() < 0.00:
            print([o[-1] for o in obs])
        for ob in obs:
            vels.append(ob[-1])
    vels = np.array(vels)
    sorted_vels = np.sort(vels)
    print('Sorted Vels')
    print(sorted_vels)

def do_training_dynamics(root_prefix, trainparams_list, inputs, targets, nprocesses):
    # use the same data (inputs, targets) to train all models
    with mp.Pool(processes=nprocesses) as pool:
        results = []
        for trainparams in trainparams_list:
            res = pool.apply_async(td.train_network, (root_prefix, trainparams, inputs, targets))
            results.append(res)
        for res in results:
            res.get()

def do_training_policies(root_prefix, trainpolparams_list, nprocesses):
    if nprocesses > 1:
        with mp.Pool(processes=nprocesses) as pool:
            results = []
            for trainpolparams in trainpolparams_list:
                res = pool.apply_async(tp.train_policy_theano, (root_prefix, trainpolparams))
                results.append(res)
            for res in results:
                res.get()
    else:
        for trainpolparams in trainpolparams_list:
            tp.train_policy_theano(root_prefix, trainpolparams)

def do_training_policies_ddpg(root_prefix, trainpolparams_list, nprocesses):
    if nprocesses > 1:
        with mp.Pool(processes=nprocesses) as pool:
            results = []
            for trainpolparams in trainpolparams_list:
                res = pool.apply_async(thd.train_policy, (root_prefix, trainpolparams))
                results.append(res)
            for res in results:
                res.get()
    else:
        for trainpolparams in trainpolparams_list:
            thd.train_policy(root_prefix, trainpolparams)

def do_training_policies_batch_ddpg(root_prefix, trainpolparams_list, trajs, nprocesses):
    if nprocesses > 1:
        with mp.Pool(processes=nprocesses) as pool:
            results = []
            for trainpolparams in trainpolparams_list:
                res = pool.apply_async(thd.train_policy_batch, (root_prefix, trainpolparams, trajs))
                results.append(res)
            for res in results:
                res.get()
    else:
        for trainpolparams in trainpolparams_list:
            thd.train_policy_batch(root_prefix, trainpolparams, trajs)

def do_evaluate(root_prefix, senv_test, trainpolparams_list, start_iter, gap, nprocesses, discount=1.0, self_eval=1, suffix=''):
    results = dict()
    tasks = []
    with mp.Pool(processes=nprocesses) as pool:
        for trainpolparams in trainpolparams_list:
            num_iter = trainpolparams.num_iter
            for i in range(start_iter, num_iter, gap):
                rewards_oracle_async = pool.apply_async(ev.test_policy, (root_prefix, trainpolparams, senv_test, discount, i, 100))
                rewards_self_async = pool.apply_async(ev.test_policy, (root_prefix, trainpolparams, trainpolparams.env, discount, i, self_eval))
                tasks.append((trainpolparams, i, rewards_self_async, rewards_oracle_async))
        for trainpolparams, i, rewards_self_async, rewards_oracle_async in tasks:
            rewards_self = rewards_self_async.get()
            rewards_oracle = rewards_oracle_async.get()
            env_name = trainpolparams.env.name()
            pol_seed = trainpolparams.seed
            results.setdefault(env_name, dict()).setdefault(pol_seed, list()).append((i, np.mean(rewards_self), np.mean(rewards_oracle)))
            
    # dump results to generic results file
    with open(os.path.join(root_prefix, 'results/results{}.pkl'.format(suffix)),'wb') as f:
#        print(results)
        pickle.dump(results, f)
    
    # print out results as well
    print_evaluate(root_prefix, suffix)

def arr2str(arr, p=2):
    return '(' + (', '.join(('{:.'+str(p)+'f}').format(x) for x in arr)) + ')'

def debug_policy(root_prefix, trainpolparams, env, policy_itr, ntrials):
    '''
    Given a Policy (rllab policy), and environment, test the policy and return stats.
    '''
    DEBUG = False
    # assume using theano and not tensorflow policies
    
    env = normalize(env)
    policy_path = tp.get_policy_path(root_prefix, trainpolparams)
    pol = joblib.load(os.path.join(policy_path, 'itr_{}.pkl'.format(policy_itr)))['policy']
    rewards = np.zeros((ntrials,)) # only care about total sums of rewards
    
    for i in range(ntrials):
        ob = env.reset()
        pol.reset()
        for t in range(trainpolparams.horizon):
            a = pol.get_action(ob)[0]
            ob, r, done, _ = env.step(a)
            rewards[i] += r
            #print(r)
            if done:
                break
        print(arr2str(ob, p=4))
    return

def print_evaluate(root_prefix, suffix):
    # dump results to generic results file
    with open(os.path.join(root_prefix, 'results/results{}.pkl'.format(suffix)),'rb') as f:
#        print(results)
        results = pickle.load(f)
    outputstr = io.StringIO()
    for ename in sorted(results.keys()):
        print('{}'.format(ename))
        for psd in sorted(results[ename].keys()):
            outputstr.write('  {:10d}:'.format(psd))
            for i, rs, ro in results[ename][psd]:
                outputstr.write(' (')
                outputstr.write('{:d}:'.format(i))
                outputstr.write((' rs|{:.3f}').format(rs))
                outputstr.write((' ro|{:.3f}').format(ro))
                outputstr.write(')'.format(ro))
            #outputstr.write('\n')
                
            all_rs = np.array([rs for _, rs, _ in results[ename][psd]])
            best_ix = np.argmax(all_rs)
            best_tuples = [results[ename][psd][ix] for ix in [best_ix]]
            outputstr.write('  Best Iterations: (')
            for i, rs, ro in best_tuples:
                outputstr.write('{:d}:'.format(i))
                outputstr.write((' rs|{:.3f}').format(rs))
                outputstr.write((' ro|{:.3f}').format(ro))
                outputstr.write(')'.format(ro))
            outputstr.write('\n')
    print(outputstr.getvalue())

def do_evaluate_ddpg(root_prefix, senv_test, trainpolparams_list, start_iter, gap, nprocesses, self_eval=2):
    '''
    pre is the precision to be used in reporting the final score
    '''
    results = dict()
    tasks = []
    with mp.Pool(processes=nprocesses) as pool:
        for trainpolparams in trainpolparams_list:
            # get the qloss
            curpath = tp.get_policy_path(root_prefix, trainpolparams)
            with open(os.path.join(curpath, 'train_stats.pkl'), 'rb') as f:
                train_stats = pickle.load(f)
            num_iter = trainpolparams.num_iter
            for i in range(start_iter, num_iter, gap):
                rewards_oracle_async = pool.apply_async(ev.test_policy, (root_prefix, trainpolparams, senv_test, i, 2))
                rewards_self_async = pool.apply_async(ev.test_policy, (root_prefix, trainpolparams, trainpolparams.env, i, self_eval))
                qloss = train_stats['qlosses'][i]
                esmean = train_stats['esmean'][i]
                esstd = train_stats['esstd'][i]
                tasks.append((trainpolparams, i, qloss, esmean, esstd, rewards_self_async, rewards_oracle_async))
        for trainpolparams, i, qloss, esmean, esstd, rewards_self_async, rewards_oracle_async in tasks:
            rewards_oracle = rewards_oracle_async.get()
            rewards_self = rewards_self_async.get()
            env_name = trainpolparams.env.name()
            pol_seed = trainpolparams.seed
            results.setdefault(env_name, dict()).setdefault(pol_seed, list()).append((i, qloss, esmean, esstd, np.mean(rewards_self), np.mean(rewards_oracle)))
            
    # dump results to generic results file
    with open(os.path.join(root_prefix, 'results.pkl'),'wb') as f:
#        print(results)
        pickle.dump(results, f)
    
    # print out results as well
    print_evaluate_ddpg(root_prefix, pres=(2,3,3,3,0))

def print_evaluate_ddpg(root_prefix, pres=(2,3,3,3,0)):
    # dump results to generic results file
    with open(os.path.join(root_prefix, 'results.pkl'),'rb') as f:
#        print(results)
        results = pickle.load(f)
    outputstr = io.StringIO()
    for ename in sorted(results.keys()):
        print('{}'.format(ename))
        for psd in sorted(results[ename].keys()):
            outputstr.write('  {:10d}:'.format(psd))
            for i, qv, esm, ess, rs, ro in results[ename][psd]:
                outputstr.write(' (')
                outputstr.write('{:d}:'.format(i))
                outputstr.write((' q|{:.'+str(pres[0])+'e}').format(qv))
                outputstr.write((' esm|{:.'+str(pres[1])+'f}').format(esm))
                outputstr.write((' ess|{:.'+str(pres[2])+'f}').format(ess))
                outputstr.write((' rs|{:.'+str(pres[3])+'f}').format(rs))
                outputstr.write((' ro|{:.'+str(pres[4])+'f}').format(ro))
                outputstr.write(')'.format(ro))
            #outputstr.write('\n')
                
            all_rs = np.array([rs for _, _, _, _, rs, _ in results[ename][psd]])
            best_ix = np.argmax(all_rs)
            best_tuples = [results[ename][psd][ix] for ix in [best_ix]]
            outputstr.write('  Best Iterations: (')
            for i, qv, esm, ess, rs, ro in best_tuples:
                outputstr.write('{:d}:'.format(i))
                outputstr.write((' q|{:.'+str(pres[0])+'e}').format(qv))
                outputstr.write((' esm|{:.'+str(pres[1])+'f}').format(esm))
                outputstr.write((' ess|{:.'+str(pres[2])+'f}').format(ess))
                outputstr.write((' rs|{:.'+str(pres[3])+'f}').format(rs))
                outputstr.write((' ro|{:.'+str(pres[4])+'f}').format(ro))
                outputstr.write(')'.format(ro))
            outputstr.write('\n')
    print(outputstr.getvalue())

def do_evaluate_ddpg_batch(root_prefix, senv_test, trainpolparams_list, start_iter, gap, nprocesses):
    '''
    pre is the precision to be used in reporting the final score
    '''
    results = dict()
    tasks = []
    with mp.Pool(processes=nprocesses) as pool:
        for trainpolparams in trainpolparams_list:
            # get the qloss
            curpath = tp.get_policy_path(root_prefix, trainpolparams)
            with open(os.path.join(curpath, 'qlosses.pkl'), 'rb') as f:
                qlosses = pickle.load(f)
            num_iter = trainpolparams.num_iter
            for i in range(start_iter, num_iter, gap):
                rewards_oracle_async = pool.apply_async(ev.test_policy, (root_prefix, trainpolparams, senv_test, i, 2))
                qloss = qlosses[i]
                tasks.append((trainpolparams, i, qloss, rewards_oracle_async))
        for trainpolparams, i, qloss, rewards_oracle_async in tasks:
            rewards_oracle = rewards_oracle_async.get()
            env_name = trainpolparams.env.name()
            pol_seed = trainpolparams.seed
            results.setdefault(env_name, dict()).setdefault(pol_seed, list()).append((i, qloss, np.mean(rewards_oracle)))
            
    # dump results to generic results file
    with open(os.path.join(root_prefix, 'results.pkl'),'wb') as f:
#        print(results)
        pickle.dump(results, f)
    
    # print out results as well
    print_evaluate_ddpg_batch(root_prefix)

def print_evaluate_ddpg_batch(root_prefix, pres=(2,0)):
    # dump results to generic results file
    with open(os.path.join(root_prefix, 'results.pkl'),'rb') as f:
#        print(results)
        results = pickle.load(f)
    outputstr = io.StringIO()
    for ename in sorted(results.keys()):
        print('{}'.format(ename))
        for psd in sorted(results[ename].keys()):
            outputstr.write('  {:10d}:'.format(psd))
            for i, qv, ro in results[ename][psd]:
                outputstr.write(' (')
                outputstr.write('{:d}:'.format(i))
                outputstr.write((' q|{:.'+str(pres[0])+'e}').format(qv))
                outputstr.write((' ro|{:.'+str(pres[1])+'f}').format(ro))
                outputstr.write(')'.format(ro))
            outputstr.write('\n')
    print(outputstr.getvalue())


def do_online_evaluate(root_prefix, senv_test, trainpolparams_seed_dict, nprocesses):
    # not sure this function is working - hasn't been updated for a while
    results = dict()
    tasks = []
    with mp.Pool(processes=nprocesses) as pool:
        for sd in trainpolparams_seed_dict:
            trainpolparams_list = trainpolparams_seed_dict[sd]
            num_iter = trainpolparams_list[0].num_iter
            for i in range(50, num_iter, 50):
                rewards_oracle_async = pool.apply_async(op.test_policy, (root_prefix, trainpolparams_list, senv_test, i, 100))
                rewards_self_async = pool.apply_async(op.test_policy, (root_prefix, trainpolparams_list, trainpolparams_list[0].env, i, 100))
                tasks.append((sd, i, rewards_self_async, rewards_oracle_async))
        for sd, i, rewards_self_async, rewards_oracle_async in tasks:
            rewards_self = rewards_self_async.get()
            rewards_oracle = rewards_oracle_async.get()
            results.setdefault(sd, list()).append((i, np.mean(rewards_self), np.mean(rewards_oracle)))
    for sd in results:
        print(sd)
        for i in results[sd]:
            print(i)


def check_mse(root_prefix, env, trainpolparams, trainparams_list, ntrials):
    '''Runs the trained policy in given env for multiple rollouts and computes MSE stats for the given list of models.'''
    DEBUG = True
    learned_envs = [dw.DynamicsWrapper(root_prefix, env, [trainparams], 'foo', dw.EnsembleAverage) for trainparams in trainparams_list]
    tfgraph = tf.Graph()
    with tf.Session(graph=tfgraph) as sess:
        env = TfEnv(normalize(env))
        policy_path = tp.get_policy_path(root_prefix, trainpolparams)
        pol = joblib.load(os.path.join(policy_path, 'itr_{}.pkl'.format(int(trainpolparams.num_iter / 50) * 50)))['policy']
        acc_mse = np.zeros((len(trainparams_list),))
        last_acc_mse = np.zeros((len(trainparams_list),))
        for i in range(ntrials):
            ob = env.reset()
            pol.reset()
            for lenv in learned_envs:
                lenv.reset()
            if DEBUG:
                print('trial {}:'.format(i))
            for t in range(trainpolparams.horizon):
                # get expert action
                a = pol.get_action(ob)[0]
                # get expert observation
                ob, r, done, _ = env.step(a)
                # compute the multistep MSE for the learned envs
                for j in range(len(trainparams_list)):
                    this_ob, _, _, _ = learned_envs[j].step(a)
                    acc_mse[j] += np.sum(np.square(this_ob - ob))
                    last_acc_mse[j] += np.sum(np.square(this_ob[-1] - ob[-1]))
                # ignore done since we only care about prediction MSE
        # average MSE per step
        acc_mse /= ntrials * trainpolparams.horizon
        last_acc_mse /= ntrials * trainpolparams.horizon
        # print out the results
        results = dict()
        for j in range(len(trainparams_list)):
            results[trainparams_list[j].name()] = (acc_mse[j], last_acc_mse[j])
        for k in sorted(results.keys()):
            print('CHECKING MSE: {} = {:.8f} {:.8f}'.format(k, results[k][0], results[k][1]))
    

def do_swimmer_stuff():
    # deprecated
    
    dparams = gd.DataParams(senv, horizon, ntraj, filter_reward)
    
    ###############################################################################
    ######## generate data if not already generated
    gd.generate_data(root_prefix, dparams)
    
    ######## load data
    trajs = gd.load_data(root_prefix, dparams)
    data_stats(trajs)
    inputs, targets = td.convert_data(trajs)
    
    
    ###############################################################################
    ######## train a bunch of policies per model
    trainpolparams_list = []
    trainpolparams_seed_dict = collections.defaultdict(list)
    start_iter = 0
    gap = 50
    for trainparams in trainparams_list:
        # load model from checkpoints as an environment
        learned_env = dw.DynamicsWrapper(root_prefix, senv, [trainparams], 'foo', dw.EnsembleAverage)
        
        for sd in policy_seeds:
            trainpolparams = tp.TrainPolicyParams(learned_env, test_horizon, trpo_num_iter, policy_batchsize, sd, 1)
            trainpolparams_list.append(trainpolparams)
            trainpolparams_seed_dict[sd].append(trainpolparams)
    #do_training_policies(root_prefix, trainpolparams_list, nprocesses=32)
    
    ######## evaluate the policy in an environment
    # run this separately from training models
    #do_evaluate(root_prefix, senv_test, trainpolparams_list, start_iter, gap, nprocesses=20)
    #do_online_evaluate(root_prefix, senv_test, trainpolparams_seed_dict, nprocesses=20)    
    
    ###############################################################################
    
    
    ######## check MSE of learned models on expert policy
    trainpolparams = tp.TrainPolicyParams(senv, test_horizon, 101, policy_batchsize, 331491256, 20)
    #check_mse(root_prefix, senv_test, trainpolparams, trainparams_list, 100)

def experiment_train_models(root_prefix, dparams, trainparams_list):
    ######## train a bunch of models
    gd.generate_data(root_prefix, dparams)
    trajs = gd.load_data(root_prefix, dparams)
    data_stats(trajs)
    if not dparams.env.is_pomdp:
        inputs, targets = td.convert_data(trajs)
    else:
        inputs, targets = tdrnn.data2rnndata(trajs)
    do_training_dynamics(root_prefix, trainparams_list, inputs, targets, nprocesses=min(len(trainparams_list),30))

def experiment_ensemble_trpo(root_prefix, dparams, policy_seeds, senv, trainparams_list, size_list, horizon, reps, train_mode=False, pbs=100, act_fn='relu', nhidden=32, num_iter=1, gap=50, eval_discount=1.0, ensemble_cls=None):
    ######## train policies on ensemble models
    # on a variety of ensemble sizes, each subsampled from the given list of models
    # and repeated multiple times
    # 1001 is probably enough for ensembles, 501 might be enough for swimmer
    trpo_num_iter = num_iter
    # seems like batch size of 500 is pretty good and needed for ensemble half cheetah, batch size of 100 fails
    # for swimmer, looks like >100 might still useful, though probably not for small ensembles
    policy_batchsize = pbs
    
    all_trainpolparams_list = []
    for ensemble_size in size_list:
        # use independent, consistent PRNG to generate ixs for ensembles
        ix_prng = np.random.RandomState(331491256+ensemble_size)
        for rep in range(reps):
            ensemble_trainpolparams_list = []
            # load model from checkpoints as an environment
            # randomly pick ensemble_size from the given list of models to use as the ensemble
            if ensemble_size > 1:
                picked_ixs = ix_prng.choice(len(trainparams_list), size=ensemble_size, replace=False)
            else:
                # special case for single sizes where we just use the rep as the index
                picked_ixs = [rep]
            print('Picked ixs {}'.format(picked_ixs))
            ensemble_trainparams_list = [trainparams_list[ix] for ix in picked_ixs]
            # random seems to be more stable for training than thompson, plus random preserves MDP rather than pomdp
            ensemble_name = 'grouprep{}'.format(rep)
            learned_env = dw.DynamicsWrapper(root_prefix, senv, ensemble_trainparams_list, ensemble_name, ensemble_cls)
            for sd in policy_seeds:
                trainpolparams = tp.TrainPolicyParams(learned_env, horizon, trpo_num_iter, policy_batchsize, act_fn, nhidden, sd, 1)
                ensemble_trainpolparams_list.append(trainpolparams)
                all_trainpolparams_list.append(trainpolparams)
            start_iter = 0
            results_suffix = '-size{}-rep{}'.format(ensemble_size, rep)
            # need lots of self evals because trpo's policy and the ensemble are both stochastic
            if not train_mode:
                do_evaluate(root_prefix, senv, ensemble_trainpolparams_list, start_iter, gap, nprocesses=min(30,len(policy_seeds)), self_eval=300, suffix=results_suffix, discount=eval_discount)
    # train all ensemble sizes and reps together in one pool
    if train_mode:
        do_training_policies(root_prefix, all_trainpolparams_list, nprocesses=min(30,len(all_trainpolparams_list)))
        pass

def experiment_ensemble_ddpg(root_prefix, dparams, policy_seeds, env, trainparams_list, horizon):
    ######## train regular ddpg policies on ensemble models
    ## outdated
    num_iter = 21
    policy_batchsize = 1000
    ensemble_trainpolparams_list = []
    # load model from checkpoints as an environment
    # random seems to be more stable for training than thompson, plus random preserves MDP rather than pomdp
    learned_env = dw.DynamicsWrapper(root_prefix, env, trainparams_list, 'group', dw.EnsembleRandom)
    for sd in policy_seeds:
        trainpolparams = thd.TrainPolicyParams(learned_env, horizon, num_iter, policy_batchsize, False, sd)
        ensemble_trainpolparams_list.append(trainpolparams)
    do_training_policies_ddpg(root_prefix, ensemble_trainpolparams_list, nprocesses=min(30, len(policy_seeds)))
    start_iter = 1
    gap = 1
    # only the ensemble is stochastic, so don't need as many self-evals as for trpo ensemble
    do_evaluate_ddpg(root_prefix, env, ensemble_trainpolparams_list, start_iter, gap, nprocesses=min(30, len(policy_seeds)), self_eval=500)

def experiment_ddpg_hindsight_batch(root_prefix, dparams, seeds, env_aug, horizon):
    ######## train a bunch of DDPG policies on batch data
    gd.generate_data(root_prefix, dparams)
    trajs = gd.load_data(root_prefix, dparams)
    
    trainpolparams_list = []
    ddpg_num_epochs = 10
    trainpolparams_seed_dict = collections.defaultdict(list)
    for sd in seeds:
        trainpolparams = thd.TrainPolicyParams(env_aug, horizon, ddpg_num_epochs, 100, True, sd)
        trainpolparams_list.append(trainpolparams)
    do_training_policies_batch_ddpg(root_prefix, trainpolparams_list, trajs, nprocesses=32)
    start_iter = 1
    gap = 1
    do_evaluate_ddpg_batch(root_prefix, env_aug, trainpolparams_list, start_iter, gap, nprocesses=30)

def experiment_env_trpo(root_prefix, seeds, env, horizon, train_mode=False, pbs=100, act_fn='relu', nhidden=32, num_iter=1, gap=50, eval_discount=1.0):
    policy_batchsize = pbs
    num_iters = num_iter
    oracle_trainpolparams_list = []
    for sd in seeds:
        trainpolparams = tp.TrainPolicyParams(env, horizon, num_iters, policy_batchsize, act_fn, nhidden, sd, 1)
        oracle_trainpolparams_list.append(trainpolparams)
    if train_mode:
        do_training_policies(root_prefix, oracle_trainpolparams_list, nprocesses=min(len(seeds), 30))
    
    ######## evaluate oracle policies
    #debug_policy(root_prefix, trainpolparams, env, num_iters-1, 10)
    if not train_mode:
        do_evaluate(root_prefix, env, oracle_trainpolparams_list, 0, gap=gap, nprocesses=30, discount=eval_discount)

def experiment_env_ddpg(root_prefix, seeds, env, horizon, train_mode=False, pbs=100, act_fn='relu', nhidden=32, num_iter=1):
    ######## train normal ddpg policies on oracle env
    oracle_trainpolparams_list = []
    num_iters = num_iter
    start_iter = 1
    gap = 1
    for sd in seeds:
        trainpolparams = thd.TrainPolicyParams(env, horizon, num_iters, pbs*horizon, act_fn, nhidden, False, sd)
        oracle_trainpolparams_list.append(trainpolparams)
    if train_mode:
        do_training_policies_ddpg(root_prefix, oracle_trainpolparams_list, nprocesses=min(30, len(seeds)))
    
    ######## evaluate oracle ddpg policies
    if not train_mode:
        #debug_policy(root_prefix, trainpolparams, env, num_iters-1, 1)
        do_evaluate_ddpg(root_prefix, env, oracle_trainpolparams_list, start_iter, gap, nprocesses=min(30, len(seeds)))

def experiment_env_ddpg_hindsight(root_prefix, seeds, env_aug, horizon, train_mode=False, pbs=100, act_fn='relu', num_iter=1):
    ######## train ddpg+hindsight policies on oracle env
    oracle_trainpolparams_list = []
    start_iter = 0
    gap = 1
    for sd in seeds:
        trainpolparams = thd.TrainPolicyParams(env_aug, horizon, num_iter, pbs*horizon, act_fn, True, sd)
        oracle_trainpolparams_list.append(trainpolparams)
    if train_mode:
        do_training_policies_ddpg(root_prefix, oracle_trainpolparams_list, nprocesses=min(30, len(seeds)))
    
    ######## evaluate oracle ddpg policies
    if not train_mode:
        do_evaluate_ddpg(root_prefix, env_aug, oracle_trainpolparams_list, start_iter, gap, nprocesses=30)

def do_experiment():
    '''
    [
    331491256, 317996, 21791086, 33835675, 6585405,
    85003283, 19922827, 28503138, 75720855, 88341058,
    92657409, 303230, 57584953, 41201072, 28386107,
    17540358, 85586490, 145616729, 152015139, 608403188,
    581981390, 34265261, 324431369, 121177333, 491687403,
    912849316, 184462122, 382923657, 93422015, 383892549,
    374270940, 974900428, 298231920, 939426454, 995461417,
    913453782, 921478568, 446164064, 388896523, 329545745,
    776484502, 334850451, 584821320, 124683310, 623418380,
    994249451, 293598528, 766160162, 830274089, 675407091,
    
    250562518, 346167074, 441516361, 64970807, 670018528,
    369843816, 154221372, 802012560, 510710029, 566297351,
    724223574, 600543360, 765480830, 482457786, 234109023,
    626804401, 394806814, 803522011, 96793077, 104848,
    691574437, 706774301, 711715375, 58401816, 236444548,
    945035728, 938293214, 895182543, 407839105, 362501316,
    629333743, 852024366, 595135232, 265174394, 143756579,
    888651646, 854442427, 182852364, 490773228, 273766330,
    22221848, 653190812, 661114719, 892456771, 779845188,
    289729862, 609669704, 396332902, 363995956, 118869516,
    ]
    '''
    
    parser = argparse.ArgumentParser(description='Run Experiments')
    parser.add_argument('--test', action='store_false', dest='train_mode', help='Enable test mode')
    parser.add_argument('env', action='store')
    
    parsed_args = parser.parse_args()
    train_mode = parsed_args.train_mode
    env_name = parsed_args.env
    
    print('Train Mode: {}'.format(train_mode))
    print('Env: {}'.format(env_name))
    
    # parameters
    root_prefix = '.'
    
    # common data generation parameters
    horizon = 20
    test_horizon = 30
    ntraj = 10000 #10000 for both swimmer and half-cheetah
    
    # common model training parameters
    nhidden = 500
    predictdiff = True
    dropout = False
    batchnorm = False
    valfrac = 0.1
    lr = 0.001 # for halfcheetah, tried 0.002 and 0.0001 and it seems like 0.001 is best
    batchsize = 128
    
    if env_name == 'swimmer':
        # trying to find the smallest goal where we don't throw away too much data
        # 0.35 seems to be good for h20 with half cheetah, needing to throw away approximately 5% of generated data
        # 0.07 seems to be good for h20 swimmer, with filtering out less than 1%, 0.10 also seems to work as a harder goal
        # tried an optimal policy for goalvel 0.1 in swimmer, and found that qpos's first element is always around 0.25, or 0.26
        # so use that as a secondary goal
        # (ix, goal, thresh)
        # sparse reward

        # after 30 steps, seems like swimmer can reach about 0.147 as the final velocity, using trpo
        # with ddpg, only can get to 0.135
        swimmer_dense_rs = [(13, 1.0)]

        # 0.11 works for 9/10 seeds and 150 iterations of trpo, but 0.12 only 1/10 seeds worked
        # 0.10 works for 7/10 seeds and 20 iterations (pbs 1000) of regular ddpg, with potentially more iterations working
        # 0.11 seems like it might work if we carefully pick the epoch that has the highest es mean maybe for regular ddpg
        # for ddpg+hindsight, 0.01 seems to work, but 0.02 just completely fails
        swimmer_goals = [(13, 0.10, 0.01)]
        # to do a joint goal, where threshold of 0.01 is hard to reach on a random policy
        #swimmer_goals = [(0, 0.26, 0.01), (13, 0.10, 0.01)]
        
        filter_reward = True
        single_reward = True
        
        nepoch = 10 # 30 for half-cheetah, 10 for swimmer, 20 for IB
        
        #senv = CustomSwimmerDenseEnv(swimmer_dense_rs, time_augmented=True)
        senv = CustomSwimmerEnv(swimmer_goals, single_reward, False)
        senv_aug = CustomSwimmerEnv(swimmer_goals, single_reward, True) # augmented with goal
        
    elif env_name == 'halfcheetah':
        halfcheetah_dense_rs = [(20, 1.0)]

        # after 30 steps, it looks like can reach about 1.2-1.6 as final velocity
        # looks like a goal of 0.8 still works with 9/10 seeds getting > 90% using 150 iterations of trpo
        # looks like a goal of 0.9 still works with 7/10 seeds getting > 90% using 150 iterations of trpo
        # looks like 0.9 works for plain ddpg as well with 10 to 20 iterations and psb 1000
        # looks like 0.7 is very hard for ddpg+hindsight, but it seems to work really well (within 1 epoch) for 0.6
        halfcheetah_goals = [(20, 0.7, 0.1)]
        
        filter_reward = True
        single_reward = True
        
        nepoch = 30 # 30 for half-cheetah, 10 for swimmer, 20 for IB
        
        #senv = CustomHalfCheetahDenseEnv(halfcheetah_dense_rs, time_augmented=True)
        senv = CustomHalfCheetahEnv(halfcheetah_goals, single_reward, False)
        senv_aug = CustomHalfCheetahEnv(halfcheetah_goals, single_reward, True) # augmented with goal
    
    elif env_name == 'ib':
        filter_reward = False
        single_reward = False
        nepoch = 20 # 30 for half-cheetah, 10 for swimmer, 20 for IB
        senv = CustomIBEnv()
        
    else:
        raise RuntimeError('Invalid environment name.')
    
    # 1000 seems okay for ddpg swimmer
    # seems like this policy batch size for ddpg is not super useful, since it just defines the epoch
    # the more important paramter is the minibatch size for ddpg, which is tuned inside the algorithm
    # for ddpg, can just do 90000/minibatchsize so each epoch is one sweep over offline data
    # for ddpg+hindsight online, use a large one like 1000 because it seems to require lots of data
    #policy_batchsize = 1000 # large batch size of 1500 seems important for half-cheetah, otherwise use normal 100
    
    model_seeds = [331491256, 317996, 21791086, 33835675, 6585405,
    85003283, 19922827, 28503138, 75720855, 88341058,
    92657409, 303230, 57584953, 41201072, 28386107,
    17540358, 85586490, 145616729, 152015139, 608403188,
    581981390, 34265261, 324431369, 121177333, 491687403,
    912849316, 184462122, 382923657, 93422015, 383892549,
    374270940, 974900428, 298231920, 939426454, 995461417,
    913453782, 921478568, 446164064, 388896523, 329545745,
    776484502, 334850451, 584821320, 124683310, 623418380,
    994249451, 293598528, 766160162, 830274089, 675407091]

    policy_seeds = [250562518, 346167074, 441516361, 64970807, 670018528,
    369843816, 154221372, 802012560, 510710029, 566297351,
    724223574, 600543360, 765480830, 482457786, 234109023,
    626804401, 394806814, 803522011, 96793077, 104848,
    691574437, 706774301, 711715375, 58401816, 236444548,
    945035728, 938293214, 895182543, 407839105, 362501316]
    
    # parameters for generating batch data for learning models or doing batch rl
    # or use a specific learned policy
    #  691574437 is best bad seed, 369843816 is worst bad seed
    badpolicypath='policysnapshots/DynamicsWrapper-CustomSwimmer-goL13-0.1-0.01J-srTrue-gaFalse-grouprep0-size50-fnTHOMPSON-testhorizon30-pbs100-afrelu-nh32-psd369843816-np1/itr_500.pkl'
    #dparams = gd.DataParams(senv, horizon, ntraj, filter_reward)
    dparams = gd.DataParams(senv, horizon, ntraj, filter_reward, policyname='badseed', policypath=badpolicypath)
    
    # parameters for training dynamics models
    trainparams_list = []
    for sd in model_seeds:
        if not senv.is_pomdp:
            netparams = td.NetworkParams(sd, nhidden, predictdiff, dropout, batchnorm)
            trainparams = td.TrainParams(dparams, netparams, td.TFMLP, nepoch, valfrac, lr, batchsize)
        else:
            netparams = tdrnn.NetworkParams(sd, nhidden, predictdiff)
            trainparams = td.TrainParams(dparams, netparams, tdrnn.GRUNetwork, nepoch, valfrac, lr, batchsize)
        trainparams_list.append(trainparams)
    
    ################# training dynamics models ####################
    experiment_train_models(root_prefix, dparams, trainparams_list)
    
    ################# train ensemble robust policies ##############
    #experiment_ensemble_trpo(root_prefix, dparams, policy_seeds, senv, trainparams_list, [50], test_horizon, 1, train_mode=train_mode, pbs=100, act_fn='relu', nhidden=32, num_iter=501, gap=100, eval_discount=1.00, ensemble_cls=dw.EnsembleThompson)
    #experiment_ensemble_ddpg(root_prefix, dparams, policy_seeds, senv, trainparams_list, test_horizon)
    
    ################## doing batch off-policy rl with ddpg+hindsight ###################
    #experiment_ddpg_hindsight_batch(root_prefix, dparams, policy_seeds, senv_aug, test_horizon)
    
    ################ testing against single (possibly oracle) envs #####################
    #experiment_env_trpo(root_prefix, seeds=policy_seeds, env=senv, horizon=test_horizon, train_mode=train_mode, pbs=100, act_fn='relu', nhidden=32, num_iter=101, gap=50, eval_discount=0.99)
    #experiment_env_ddpg(root_prefix, seeds=policy_seeds, env=senv, horizon=test_horizon, train_mode=train_mode, pbs=100, act_fn='relu', num_iter=15)
    #experiment_env_ddpg_hindsight(root_prefix, seeds=policy_seeds, env_aug=senv_aug, horizon=test_horizon)
    

def main():
    starttime = time.time()
    do_experiment()
    print('Elapsed {:.3f}s'.format(time.time() - starttime))


if __name__ == '__main__':
    main()
