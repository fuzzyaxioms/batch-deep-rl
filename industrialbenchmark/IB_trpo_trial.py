import os
import sys
from IB_trpo import run
from evaluate import test_policy
from industrialbenchmark.industrial_benchmark_openai.rllab_IB import rllab_IB
import numpy as np

SEEDS = [535724047,
         419754265,
         195210748,
         539359910,
         358556062,
         350459082,
         62167038,
         445770914,
         415431246,
         467815816]

HORIZONS = [10, 20, 30, 50, 100, 200, 300] #not used currently

horizon = int(sys.argv[1])
# i = int(sys.argv[2])

# to edit before experiments
main_directory = "IB_trpo_snapshots"
job_id = "mlpmarkov"
policy = "MLP"
gap = 100
n_seeds = 10
n_itr = 601
policy_itr = n_itr - 1
test_horizon = 300
n_trials = 100
markov_state=True

# set up main directory for TRPO snapshots
if not os.path.exists(main_directory):
    os.makedirs(main_directory)

# seed = SEEDS[i]
for i, seed in enumerate(SEEDS):
    # set up sub directory for current TRPO model snapshots
    directory = main_directory + "/"
    directory += "horiz" + str(horizon) + "seed" + str(i)
    directory += job_id

    if not os.path.exists(directory):
        os.makedirs(directory)

    run(horizon=horizon, setpoint=50, reward_type="classic", action_type="continuous", 
        stationary_p=True, markov_state=markov_state, n_itr=n_itr, seed=seed, 
        snapshot_dir=directory, policy=policy, gap=gap)
    
    env = rllab_IB(horizon=test_horizon, setpoint=50, reward_type="classic", action_type="continuous",
                   stationary_p=True, markov_state=markov_state)
    
    results = test_policy(policy_path=directory, horizon=test_horizon, env=env, policy_itr=policy_itr, 
                          ntrials=n_trials)

    np.save(directory + "/results", results)
