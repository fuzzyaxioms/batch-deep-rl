'''Evaluate policies in environments.'''
from sandbox.rocky.tf.policies.gaussian_mlp_policy import GaussianMLPPolicy
from sandbox.rocky.tf.envs.base import TfEnv
from rllab.envs.normalized_env import normalize

# import train_policies as tp

import pickle
import os
import joblib

import numpy as np
import tensorflow as tf

# def np2str(arr):
#     return ' '.join('{:.4f}'.format(x) for x in arr)

# def test_policy(root_prefix, trainpolparams, env, policy_itr, ntrials):
def test_policy(policy_path, horizon, env, policy_itr, ntrials):
    '''
    Given a Policy (rllab policy), and environment, test the policy and return stats.
    '''
    DEBUG = False
    
    env = normalize(env)
#     policy_path = tp.get_policy_path(root_prefix, trainpolparams)
    pol = joblib.load(os.path.join(policy_path, 'itr_{}.pkl'.format(policy_itr)))['policy']
    rewards = np.zeros((ntrials,)) # only care about total sums of rewards
    
    for i in range(ntrials):
        ob = env.reset()
        pol.reset()
        for t in range(horizon):
            a = pol.get_action(ob)[0]
            ob, r, done, _ = env.step(a)
            rewards[i] += r
            if done:
                break

    return rewards

def test_policy_array(policy_path, horizon, env, policy_itr, ntrials):
    '''
    Given a Policy (rllab policy), and environment, test the policy and return stats.
    '''
    DEBUG = False
    
    env = normalize(env)
#     policy_path = tp.get_policy_path(root_prefix, trainpolparams)
    pol = joblib.load(os.path.join(policy_path, 'itr_{}.pkl'.format(policy_itr)))['policy']
    rewards = np.zeros((ntrials,horizon)) # only care about total sums of rewards
    
    for i in range(ntrials):
        ob = env.reset()
        pol.reset()
        for t in range(horizon):
            a = pol.get_action(ob)[0]
            ob, r, done, _ = env.step(a)
            rewards[i,t] = r
            if done:
                break

    return rewards