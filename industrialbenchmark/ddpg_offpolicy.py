from rllab.algos.ddpg import DDPG
from rllab.exploration_strategies.ou_strategy import OUStrategy
from rllab.exploration_strategies.gaussian_strategy import GaussianStrategy
from rllab.envs.normalized_env import normalize
import rllab.misc.logger as logger
from rllab.misc.ext import set_seed
from rllab.policies.deterministic_mlp_policy import DeterministicMLPPolicy
from rllab.q_functions.continuous_mlp_q_function import ContinuousMLPQFunction

# import generate_data as gd
# import train_policies as tp
# import pyprind

import time
import pickle
import itertools
import os
import numpy as np

from industrialbenchmark.industrial_benchmark_openai.rllab_IB import rllab_IB

class CustomSimpleReplayPool(object):
    '''
    Edited to also contain next_ob as part of the data for convenience even though it is redundant.
    '''
    def __init__(
            self, max_pool_size, observation_dim, action_dim):
        self._observation_dim = observation_dim
        self._action_dim = action_dim
        self._max_pool_size = max_pool_size
        self._observations = np.zeros(
            (max_pool_size, observation_dim),
        )
        self._next_observations = np.zeros(
            (max_pool_size, observation_dim),
        )
        self._actions = np.zeros(
            (max_pool_size, action_dim),
        )
        self._rewards = np.zeros(max_pool_size)
        self._terminals = np.zeros(max_pool_size, dtype='uint8')
        self._bottom = 0
        self._top = 0
        self._size = 0
    
    def clear(self):
        '''
        Clear the pool of all samples.
        '''
        self._bottom = 0
        self._top = 0
        self._size = 0
        

    def add_sample(self, observation, action, next_observation, reward, terminal):
        self._observations[self._top] = observation
        self._next_observations[self._top] = next_observation
        self._actions[self._top] = action
        self._rewards[self._top] = reward
        self._terminals[self._top] = terminal
        self._top = (self._top + 1) % self._max_pool_size
        if self._size >= self._max_pool_size:
            self._bottom = (self._bottom + 1) % self._max_pool_size
        else:
            self._size += 1

    def random_batch(self, batch_size):
        assert self._size > batch_size
        indices = np.zeros(batch_size, dtype='uint64')
        count = 0
        while count < batch_size:
            index = np.random.randint(self._bottom, self._bottom + self._size) % self._max_pool_size
            # make sure that the transition is valid: if we are at the end of the pool, we need to discard
            # this sample
            if index == self._size - 1 and self._size <= self._max_pool_size:
                continue
            # if self._terminals[index]:
            #     continue
            indices[count] = index
            count += 1
        return dict(
            observations=self._observations[indices],
            actions=self._actions[indices],
            rewards=self._rewards[indices],
            terminals=self._terminals[indices],
            next_observations=self._next_observations[indices]
        )

    @property
    def size(self):
        return self._size

def train_policy_batch(trajs, num_iter = 10, test_horizon = 30, seed = 1, 
                       snapshot_dir="/home/jeanbetterton/projects/batch-deep-rl/industrialbenchmark/IB_ddpg_snapshots", gap=1,
                       setpoint=50, reward_type="classic", action_type="continuous",
                       stationary_p=True, markov_state=True, horizon=30,
                       policylayer1=32, policylayer2=32, qlayer1=32, qlayer2=32):
    '''
    Custom training function for DDPG+Hindsight on a batch of data.
    Should be passed the oracle env to get the env_spec
    '''
    # note that the learned policy assumes a normalized environment
    
    set_seed(seed)
    curpath = snapshot_dir # TODO
    trainstatspath = os.path.join(curpath, 'train_stats.csv') # training stats table
    logger.set_snapshot_dir(curpath)
    logger.set_snapshot_gap(gap)
    logger.set_snapshot_mode('gap')
    logger.add_tabular_output(trainstatspath)
    
    env = normalize(rllab_IB(setpoint=setpoint, reward_type=reward_type, action_type=action_type,
                             stationary_p=stationary_p, markov_state=markov_state, horizon=horizon))
    env.reset() # loads dynamic wrapper if hasn't been loaded before

#     from rllab.envs.gym_env import GymEnv
#     env = normalize(GymEnv("Pendulum-v0",  record_video=False, record_log=False))
#     env.reset()
    
    policy = DeterministicMLPPolicy(
        env_spec=env.spec,
        hidden_sizes=(policylayer1, policylayer2)
    )
    es = OUStrategy(env_spec=env.spec)
    # looks like the qfunction size is really important
    # 500,500 seems to work well for swimmer
    # 1000,1000 seems to work well for halfcheetah
    qf = ContinuousMLPQFunction(env_spec=env.spec, hidden_sizes=(qlayer1, qlayer2),)
    
    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=32,
        max_path_length=30,
        epoch_length=1000,
        min_pool_size=10000,
        n_epochs=num_iter,
        discount=0.99, # use 0.95 maybe for hopefully more stable q-learning in batch
        scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
        eval_samples=10*test_horizon,
    )
    
    # don't call algo.train, instead implement custom train
    # code initially copied from algo.train method
    # This seems like a rather sequential method
    pool = CustomSimpleReplayPool(
        max_pool_size=algo.replay_pool_size,
        observation_dim=algo.env.observation_space.flat_dim, # assume env is goal augmented
        action_dim=algo.env.action_space.flat_dim,
    )
    
    algo.start_worker()

    algo.init_opt()
    itr = 0
    # keep track of the average q loss per epoch as a sign of convergence?
    qlosses = []
    for epoch in range(algo.n_epochs):
        starttime = time.time()
        logger.push_prefix('epoch #%d | ' % epoch)
        logger.log("Training started")
        
        # either update every epoch or just once at the beginning
        if False or epoch == 0:
            logger.log("Generating replay buffer with goals")
            # generate new goals and entire new replay buffer every epoch
            pool.clear()
            # populate the pool fully with the goal augmented data
            # also must terminate trajectories properly
            for i in range(len(trajs)):
                obs, acts, rewards = trajs[i]
                # first add with original goal
                for t in range(len(acts)):
                    curr_obs = obs[t]
                    next_obs = obs[t+1]
                    reward = rewards[t] # TODO reward
                    terminal = (t+1 == len(acts)) # TODO true/false
                    pool.add_sample(curr_obs, acts[t], next_obs, reward * algo.scale_reward, terminal)
        
        logger.log("Starting mini-batch updates")
        for epoch_itr in range(algo.epoch_length):
            for update_itr in range(algo.n_updates_per_sample):
                # Train policy
                batch = pool.random_batch(algo.batch_size)
                algo.do_training(itr, batch)

            itr += 1

        logger.log("Training finished after {:.2f}s".format(time.time() - starttime))
        qlosses.append(np.mean(algo.qf_loss_averages))
        algo.evaluate(epoch, pool)
        
        # save policy snapshot
        params = algo.get_epoch_snapshot(epoch)
        logger.save_itr_params(epoch, params)
        logger.dump_tabular(with_prefix=False)
        logger.pop_prefix()
    # also save the qlosses
    logger.remove_tabular_output(trainstatspath)
    with open(os.path.join(curpath, 'qlosses.pkl'), 'wb') as f:
        pickle.dump(qlosses, f)
        
    algo.env.terminate()
    algo.policy.terminate()