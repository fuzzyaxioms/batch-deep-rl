import os
import sys
from IB_ddpg import run
from evaluate import test_policy
import numpy as np

SEEDS = [3227867, 9783120, 6695718, 2713957, 8504764, 8074860, 7932843,
            810643, 9962626, 1049227, 4797000,  968102, 3137880, 7273029,
           6856554, 2859196, 2982809, 2381462, 5757916, 4654623, 9547839,
           4861627, 7769690, 3578612,  551458, 2338356, 8450823, 8156704,
           3461397,  781488, 7486355, 1954896, 9372199, 6528741,  165732,
           5132191, 9330500, 3797837, 5404854, 8913488, 4965277, 3679131,
           3265610, 3466471, 9699670, 7323854, 7267205, 8289921, 7071391,
           2140635]


i = int(sys.argv[1])
jobs_per = int(sys.argv[2])
horizon = int(sys.argv[3])
markov_state_int = int(sys.argv[4]) #if markov state int is 0, no markov state

first_seed = i*jobs_per
last_seed = first_seed + jobs_per
seed_inds = range(first_seed, last_seed)

# to edit before experiments
main_directory = "IB_ddpg_snapshots"
job_id = "online"
gap = 100
n_itr = 1001
markov_state = (markov_state_int != 0) #if markov state int is 0, no markov state
if markov_state:
    job_id = "markov" + job_id


policy_itr = n_itr - 1 # not implemented yet
test_horizon = 300 # not implemented yet
n_trials = 100  # not implemented yet

# set up main directory for TRPO snapshots
if not os.path.exists(main_directory):
    os.makedirs(main_directory)

for ind in seed_inds:
    if ind >= len(SEEDS):
        break
        
    np.random.seed(SEEDS[ind])

    # set up sub directory for current DDPG model snapshots
    snapshot_dir = os.path.join(main_directory, "horiz" + str(horizon) + "seed" + str(ind) + job_id)    
    if not os.path.exists(snapshot_dir):
        os.makedirs(snapshot_dir)
    
    # note test horizon and environment horizon are always the same
    run(horizon=horizon, markov_state=markov_state, n_itr=n_itr, seed=SEEDS[ind], snapshot_dir=snapshot_dir,
        gap=gap)


