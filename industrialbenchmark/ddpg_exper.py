from IB_interface import generate_batch
from IB_interface import finale_policy
from IB_interface import uniform_policy
from ddpg_offpolicy import train_policy_batch
from rllab.envs.box2d.cartpole_env import CartpoleEnv
from rllab.envs.normalized_env import normalize
from rllab.envs.gym_env import GymEnv
import numpy as np
import sys
import os

SEEDS =[563619356,  59862599, 686178772, 999173443, 354200131, 324292785,
       900940481, 878443337, 788117536, 963684519, 661654157, 361911482,
        60206282, 313274798, 403090925, 468932809, 304229264, 983825971,
       933514667, 143286543, 774541669, 412776251, 976191546, 831666643,
       601746344, 967905780, 891380806,  28588462, 153356143, 861441554,
       489550972, 235914313, 289342584, 429759761, 715503368, 929768365,
       180700906,  88026598, 481527697, 520476533, 867594989, 952404885,
       954808993, 132922981, 535959471, 699051069, 917647715, 593657558,
        88755791,  66601332]

i = int(sys.argv[1])
jobs_per = int(sys.argv[2])
horizon = int(sys.argv[3])
markov_state_int = int(sys.argv[4]) #if markov state int is 0, no markov state
policylayer = 300
qlayer = 300

first_seed = i*jobs_per
last_seed = first_seed + jobs_per
seed_inds = range(first_seed, last_seed)

# to edit before experiments
main_directory = "IB_ddpg_snapshots"
job_id = "hlayer300"
gap = 100
n_itr = 1001
markov_state = (markov_state_int != 0) #if markov state int is 0, no markov state
n_trajectories = 10000
if markov_state:
    job_id = "markov" + job_id


policy_itr = n_itr - 1 # not implemented yet
test_horizon = 300 # not implemented yet
n_trials = 100  # not implemented yet


# set up main directory for TRPO snapshots
if not os.path.exists(main_directory):
    os.makedirs(main_directory)

np.random.seed(430624304)
batch = generate_batch(n_trajectories = n_trajectories, T = horizon, policy=uniform_policy, 
                       setpoint=50, reward_type="classic", action_type="continuous", stationary_p=True,
                       markov_state=markov_state)
    
for ind in seed_inds:
    if ind >= len(SEEDS):
        break
        
    # set up sub directory for current DDPG model snapshots
    snapshot_dir = os.path.join(main_directory, "horiz" + str(horizon) + "seed" + str(ind) + job_id)    
    if not os.path.exists(snapshot_dir):
        os.makedirs(snapshot_dir)
    
    # note test horizon and environment horizon are always the same
    train_policy_batch(batch, num_iter = n_itr, test_horizon = horizon, seed = SEEDS[ind], 
                       snapshot_dir=snapshot_dir, 
                       gap=gap, setpoint=50, reward_type="classic", action_type="continuous",
                       stationary_p=True, markov_state=markov_state, horizon=horizon,
                       policylayer1=policylayer, policylayer2=policylayer, qlayer1=qlayer, qlayer2=qlayer)


# env = normalize(GymEnv("Pendulum-v0", record_video=False, record_log=False))

