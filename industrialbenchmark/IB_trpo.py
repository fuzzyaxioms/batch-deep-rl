from industrialbenchmark.industrial_benchmark_openai.rllab_IB import rllab_IB
import os

from rllab.algos.trpo import TRPO
from rllab.baselines.linear_feature_baseline import LinearFeatureBaseline
from rllab.envs.box2d.cartpole_env import CartpoleEnv
from rllab.envs.normalized_env import normalize
from rllab.policies.gaussian_gru_policy import GaussianGRUPolicy
from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
from rllab.optimizers.conjugate_gradient_optimizer import ConjugateGradientOptimizer, FiniteDifferenceHvp
from rllab.misc import logger
from rllab.misc.ext import set_seed
from rllab.sampler import parallel_sampler

def run(horizon=10, setpoint=50, reward_type="classic", action_type="continuous", stationary_p=True,
        markov_state=False, n_itr=1000, seed=1, snapshot_dir="", policy="GRU", gap=10):
    
    env = normalize(rllab_IB(setpoint=setpoint, reward_type=reward_type, action_type=action_type,
                             stationary_p=stationary_p, markov_state=markov_state, horizon=horizon))
    
    policy = GaussianGRUPolicy(
        env_spec=env.spec,
    )
    if policy == "MLP":
        policy = GaussianMLPPolicy(
            env_spec=env.spec,
        )
        
    baseline = LinearFeatureBaseline(env_spec=env.spec)

    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=4000,
        max_path_length=horizon+1,
        n_itr=n_itr,
        discount=0.99,
        step_size=0.01,
        optimizer=ConjugateGradientOptimizer(hvp_approach=FiniteDifferenceHvp(base_eps=1e-5))
    )

    initial_seed = seed
    set_seed(initial_seed)

    logger.set_snapshot_dir(snapshot_dir)
    logger.set_snapshot_gap(gap)
    logger.set_snapshot_mode('gap')

    parallel_sampler.initialize(n_parallel=1)
    parallel_sampler.set_seed(initial_seed)

    logger_file = os.path.join(snapshot_dir, "train_stats.csv")
    logger.add_tabular_output(logger_file)
    algo.train()
    logger.remove_tabular_output(logger_file)
