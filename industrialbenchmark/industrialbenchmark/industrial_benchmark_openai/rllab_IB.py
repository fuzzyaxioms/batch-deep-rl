from .OpenAI_IB import OpenAI_IB

from rllab.envs.base import Env
from rllab.spaces import Box
import numpy as np


class rllab_IB(Env):
    def __init__(self, setpoint=50, reward_type="classic", action_type="continuous", stationary_p=True, markov_state=False, horizon=1000):
        self.IB = OpenAI_IB(setpoint, reward_type, action_type, stationary_p, markov_state, horizon)
        self.setpoint = setpoint
        self.reward_type = reward_type
        self.action_type = action_type
        self.stationary_p = stationary_p
        self.markov_state = markov_state
        self.horizon_val = horizon

    def step(self, action):
        """
        Run one timestep of the environment's dynamics. When end of episode
        is reached, reset() should be called to reset the environment's internal state.
        Input
        -----
        action : an action provided by the environment
        Outputs
        -------
        (observation, reward, done, info)
        observation : agent's observation of the current environment
        reward [Float] : amount of reward due to the previous action
        done : a boolean, indicating whether the episode has ended
        info : a dictionary containing other diagnostic information from the previous action
        """
        return self.IB.step(action)

    def reset(self):
        """
        Resets the state of the environment, returning an initial observation.
        Outputs
        -------
        observation : the initial observation of the space. (Initial reward is assumed to be 0.)
        """
        return self.IB.reset()

    @property
    def action_space(self):
        """
        Returns a Space object
        :rtype: rllab.spaces.base.Space
        """
        # from OpenAI_IB.py
        # spaces.Box(np.array([-1,-1,-1]), np.array([+1,+1,+1]))
        return Box(low=np.array([-1,-1,-1]), high=np.array([+1,+1,+1]))

    @property
    def observation_space(self):
        """
        Returns a Space object
        :rtype: rllab.spaces.base.Space
        """
        # from OpenAI_IB.py
        # self.observation_space = spaces.Box(low=np.array([0, 0, 0, 0, 0, 0]), high=np.array([100, 100, 100, 1000, 1000, 1000]))
        if self.markov_state:
            return Box(low=np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -6, 0, 0]), 
                       high=np.array([100, 100, 100, 100, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1, 1, 6, 5, 5]))
        else:
            return Box(low=np.array([0, 0, 0, 0, 0, 0]), 
                       high=np.array([100, 100, 100, 1000, 1000, 1000]))

    # Helpers that derive from Spaces
    # @property
    # def action_dim(self):
    #     return self.action_space.flat_dim

    # def render(self):
    #     pass

    # def log_diagnostics(self, paths):
    #     """
    #     Log extra information per iteration based on the collected paths
    #     """
    #     pass

    # @cached_property
    # def spec(self):
    #     return EnvSpec(
    #         observation_space=self.observation_space,
    #         action_space=self.action_space,
    #     )

    @property
    def horizon(self):
        """
        Horizon of the environment, if it has one
        """
        return self.horizon_val

    # def terminate(self):
    #     """
    #     Clean up operation,
    #     """
    #     pass

    # def get_param_values(self):
    #     return None

    # def set_param_values(self, params):
    #     pass

    def __getstate__(self):
        return (self.setpoint, self.reward_type, self.action_type, self.stationary_p, self.markov_state, self.horizon_val)

    def __setstate__(self, state):
        if len(state) == 4: # support for previous implementation
            self.setpoint, self.reward_type, self.action_type, self.horizon_val = state
            self.stationary_p=True
            self.markov_state=False
        else:
            self.setpoint, self.reward_type, self.action_type, self.stationary_p, self.markov_state, self.horizon_val = state
        self.IB = OpenAI_IB(self.setpoint, self.reward_type, self.action_type, self.stationary_p, self.markov_state, self.horizon_val)




