from industrialbenchmark.industrial_benchmark_openai.OpenAI_IB import OpenAI_IB
import numpy as np

# works in MDP and POMDP cases:
# Observation indices (used only in finale policy
P = 0
V = 1
G = 2
H = 3
C = 4
F = 5

# Action indicies (currently not used)
DELTA_V = 0
DELTA_G = 1
DELTA_H = 2
    
def finale_policy(state):
    z_v, z_g = np.random.normal(0.5, 1 / (3 ** 0.5), (2, 1))
    u_v, u_g, u_s = np.random.uniform(-1, 1, (3, 1))
    delta_v = 0
    if state[V] < 40:
        delta_v = z_v
    elif state[V] > 60:
        delta_v = -z_v
    else:
        delta_v = u_v
    delta_g = 0
    if state[G] < 40:
        delta_g = z_g
    elif state[G] > 60:
        delta_g = -z_g
    else:
        delta_g = u_g
    delta_s = u_s
    return (delta_v[0], delta_g[0], delta_s[0])

def uniform_policy(state, low=[-1,-1,-1], high=[1,1,1]):
    delta_v = np.random.uniform(low[0], high[0])
    delta_g = np.random.uniform(low[1], high[1])
    delta_s = np.random.uniform(low[2], high[2])
    return (delta_v, delta_g, delta_s)

def generate_batch_matrix(n_trajectories = 1000, T = 30, policy=uniform_policy, 
                   setpoint=50, reward_type="classic", action_type="continuous", stationary_p=True,
                   markov_state=False):
    if markov_state:
        n_obs = 21
    else:
        n_obs = 6
    n_act = 3
    obs_indices = range(1, n_obs + 1)
    act_indices = range(n_obs + 1, n_obs + n_act + 1)
    data = np.zeros((n_trajectories * T, n_obs + n_act + 2))
    count = 0
    for k in range(n_trajectories):
        env = OpenAI_IB(setpoint, reward_type, action_type, stationary_p, markov_state, horizon=T)
        for t in range(T):
            o_t = env.observation
            a_t = policy(o_t)
            r_t = env.reward
            data[count,0] = k + 1
            data[count, obs_indices] = o_t
            data[count, act_indices] = a_t
            data[count,-1] = r_t
            env.step(a_t)
            count += 1
    return data

def generate_batch(n_trajectories = 1000, T = 30, policy=uniform_policy, 
                   setpoint=50, reward_type="classic", action_type="continuous", stationary_p=True,
                   markov_state=False):
    env = OpenAI_IB(setpoint, reward_type, action_type, stationary_p, markov_state, horizon=T)
    n_obs = env.observation_space.shape[0]
    n_act = env.action_space.shape[0]
    trajectories = []
    for k in range(n_trajectories):
        observations = np.zeros((T+1, n_obs))
        actions = np.zeros((T, n_act))
        rewards = np.zeros(T)
        observations[0] = env.reset()
        for t in range(T):
            actions[t] = policy(observations[t])
#           actions[t] = env.action_space.sample()
            observations[t+1], rewards[t], _, _ = env.step(actions[t])
        trajectories.append((observations, actions, rewards))
    return trajectories

