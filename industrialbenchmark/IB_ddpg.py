from industrialbenchmark.industrial_benchmark_openai.rllab_IB import rllab_IB
import os

from rllab.algos.trpo import TRPO
from rllab.baselines.linear_feature_baseline import LinearFeatureBaseline
from rllab.envs.box2d.cartpole_env import CartpoleEnv
from rllab.envs.normalized_env import normalize
from rllab.policies.gaussian_gru_policy import GaussianGRUPolicy
from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
from rllab.optimizers.conjugate_gradient_optimizer import ConjugateGradientOptimizer, FiniteDifferenceHvp
from rllab.misc import logger
from rllab.misc.ext import set_seed
from rllab.sampler import parallel_sampler

from rllab.algos.ddpg import DDPG
from rllab.exploration_strategies.ou_strategy import OUStrategy
from rllab.exploration_strategies.gaussian_strategy import GaussianStrategy
from rllab.policies.deterministic_mlp_policy import DeterministicMLPPolicy
from rllab.q_functions.continuous_mlp_q_function import ContinuousMLPQFunction

def run(horizon=10, setpoint=50, reward_type="classic", action_type="continuous", stationary_p=True,
        markov_state=False, n_itr=1000, seed=1, snapshot_dir="", gap=10,
        policylayer1=32, policylayer2=32, qlayer1=32, qlayer2=32):
    
    env = normalize(rllab_IB(setpoint=setpoint, reward_type=reward_type, action_type=action_type,
                             stationary_p=stationary_p, markov_state=markov_state, horizon=horizon))
    
    policy = DeterministicMLPPolicy(
        env_spec=env.spec,
        hidden_sizes=(policylayer1, policylayer2)
    )
    es = OUStrategy(env_spec=env.spec)
    # looks like the qfunction size is really important
    # 500,500 seems to work well for swimmer
    # 1000,1000 seems to work well for halfcheetah
    qf = ContinuousMLPQFunction(env_spec=env.spec, hidden_sizes=(qlayer1, qlayer2),)
    
    test_horizon = 30
    
    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=32,
        max_path_length=horizon+1,
        epoch_length=1000,
        min_pool_size=10000,
        n_epochs=n_itr,
        discount=0.99, # use 0.95 maybe for hopefully more stable q-learning in batch
        scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
        eval_samples=10*test_horizon,
    )

    initial_seed = seed
    set_seed(initial_seed)

    logger.set_snapshot_dir(snapshot_dir)
    logger.set_snapshot_gap(gap)
    logger.set_snapshot_mode('gap')

    parallel_sampler.initialize(n_parallel=1)
    parallel_sampler.set_seed(initial_seed)

    logger_file = os.path.join(snapshot_dir, "train_stats.csv")
    logger.add_tabular_output(logger_file)
    algo.train()
    logger.remove_tabular_output(logger_file)

