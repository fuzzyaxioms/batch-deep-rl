'''Train recurrent dynamics models of environments.'''

import generate_data as gd
import train_dynamics as td
import time
import numpy as np
import os
import itertools

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

checkpointpath = 'checkpoints'

class NetworkParams(object):
    '''Hyperparameters of the function approximator'''
    def __init__(self, seed, nhidden, predictdiff):
        self.seed = seed
        self.nhidden = nhidden
        self.predictdiff = predictdiff
    
    def name(self):
        return 'gru-sd{}-nh{}-pd{}'.format(self.seed, self.nhidden, self.predictdiff)

class GRUModel(nn.Module):
    '''
    The recurrent neural network we're using to learn a recurrent dynamics model.
    The module part expects entire trajectories as inputs and outputs.
    But I also put additional methods that can do one step prediction.
    '''

    def __init__(self, input_size, output_size, params):
        super(GRUModel,self).__init__()
        self.params = params
        self.input_size = input_size
        self.output_size = output_size
        # the initial zero hidden state
        self.initialh = Variable(torch.zeros(params.nhidden), requires_grad=False)
        # the gru part
        self.rnncell = nn.GRUCell(input_size, params.nhidden)
        nn.init.constant(self.rnncell.bias_ih,0.0)
        nn.init.constant(self.rnncell.bias_hh,0.0)
        nn.init.xavier_normal(self.rnncell.weight_ih)
        nn.init.xavier_normal(self.rnncell.weight_hh)
        
        # linear output layer
        self.output_layer = nn.Linear(params.nhidden,output_size)
        nn.init.constant(self.output_layer.bias, 0.0)
        nn.init.xavier_normal(self.output_layer.weight)

    
    def forward_step(self, x, h):
        # given input and hidden for one step and one trajectory, return the output and next hidden
        # this will probably be used during prediction, when actually using this as a model
        # x and h are single dimensional
        # also returns single dimensional vectors
        h2 = self.rnncell(x.unsqueeze(0), h.unsqueeze(0))
        out = self.output_layer(h2)
        if self.params.predictdiff:
            out = torch.add(out, x[:self.output_size])
        return out.data.squeeze(), h2.squeeze()

    def forward(self, tau):
        # tau is a batch of trajectories
        # tau is [batch, timesteps, inputsize]
        # final output is [batchsize, timesteps, ouputsize] of linear outputs
        timesteps = tau.size()[1]
        batch_size = tau.size()[0]
        outputs = []
        memory = self.initialh.unsqueeze(0).expand(batch_size,self.params.nhidden)
        for i in range(timesteps):
            memory = self.rnncell(tau[:,i,:], memory)
            out = self.output_layer(memory) # [batch size, output size]
            if self.params.predictdiff:
                # add back the input to only predict the difference
                # truncate input to the same size as the output, removing elements at the end
                out = torch.add(out, tau[:,i,:self.output_size])
            outputs.append(out) # [batch_size, outputsize]
        return torch.stack(outputs,dim=1)

class GRUNetwork(td.Network):
    '''GRU network'''
    
    def initialize(self, trainparams):
        self.trainparams = trainparams
        self.lr = trainparams.lr
        self.netparams = trainparams.netparams
        self.model = GRUModel(trainparams.nfeatures, trainparams.noutputs, trainparams.netparams)
        self.optim = torch.optim.Adam(self.model.parameters(), lr=trainparams.lr)
        self.criterion = torch.nn.MSELoss()
        
    def train_batch(self, inputs, targets):
        inputs, targets = Variable(torch.from_numpy(inputs)), Variable(torch.from_numpy(targets))
        self.optim.zero_grad()
        outputs = self.model.forward(inputs)
        loss = self.criterion(outputs, targets) / (inputs.size()[1]*inputs.size()[2])
        loss.backward()
        self.optim.step()
        return loss.data[0]

    def test_batch(self, inputs, targets):
        inputs, targets = Variable(torch.from_numpy(inputs), volatile=True), Variable(torch.from_numpy(targets), volatile=True)
        outputs = self.model.forward(inputs)
        loss = self.criterion(outputs, targets) / (inputs.size()[1]*inputs.size()[2])
        return loss.data[0]
    
    def reset(self):
        '''Reset hidden state to zero'''
        self.curr_state = Variable(self.model.initialh.data, volatile=True)
    
    def predict_step(self, input_single):
        '''Take a step and return predicted next state'''
        curr_outputs, self.curr_state = self.model.forward_step(Variable(torch.FloatTensor(input_single), volatile=True),self.curr_state)
        return curr_outputs.numpy()
    
    def update_history(self, input_single):
        '''Update history without returning prediction'''
        self.predict_step(input_single)

    def save_checkpoint(self, root_prefix, suffix):
        global checkpointpath
        savepath = os.path.join(root_prefix, checkpointpath, self.trainparams.name()+suffix)
        torch.save(self.model.state_dict(), savepath)
    
    def load_checkpoint(self, root_prefix, suffix):
        # initialize first, then load
        global checkpointpath
        savepath = os.path.join(root_prefix, checkpointpath, self.trainparams.name()+suffix)
        self.model.load_state_dict(torch.load(savepath))

def data2rnndata(data):
    '''
    Generates gru compatible inputs and targets from IB.
    '''
    ndata = len(data)
    inputs_list = []
    targets_list = []
    for i in range(ndata):
        obs, acts, _ = data[i]
        curr_traj_inputs = []
        curr_traj_targets = []
        for t in range(len(acts)):
            curr_traj_inputs.append(np.concatenate([obs[t], acts[t]]))
            curr_traj_targets.append(obs[t+1])
        inputs_list.append(curr_traj_inputs)
        targets_list.append(curr_traj_targets)
    return np.array(inputs_list, dtype=np.float32), np.array(targets_list, dtype=np.float32)