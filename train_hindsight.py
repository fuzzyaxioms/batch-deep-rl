'''
DDPG+Hindsight on off-policy batch data.
'''
from rllab.algos.ddpg import DDPG, SimpleReplayPool
from rllab.exploration_strategies.ou_strategy import OUStrategy
from rllab.exploration_strategies.gaussian_strategy import GaussianStrategy
from rllab.envs.normalized_env import normalize
import rllab.misc.logger as logger
from rllab.misc.ext import set_seed
from rllab.policies.deterministic_mlp_policy import DeterministicMLPPolicy
from rllab.q_functions.continuous_mlp_q_function import ContinuousMLPQFunction
import lasagne.nonlinearities as NL

import generate_data as gd
import train_policies as tp
import tensorflow as tf
import pyprind

import time
import pickle
import itertools
import os
import numpy as np

snapshotpath = 'policysnapshots'

class TrainPolicyParams(object):
    def __init__(self, env, horizon, num_iter, batchsize, act_fn, nhidden, hindsight, seed):
        # if using hindsight, should also use the augmented env
        self.env = env
        self.horizon = horizon
        self.num_iter = num_iter
        self.batchsize = batchsize
        self.act_fn = act_fn # string name of relu or tanh
        self.nhidden = nhidden
        assert(self.act_fn == 'relu' or self.act_fn == 'tanh')
        self.hindsight = hindsight # whether to use hindsight replay
        self.seed = seed
    
    def name(self):
        return '{}-testhorizon{}-psd{}-pbs{}-af{}-nh{}-hd{}-ddpg'.format(self.env.name(), self.horizon, self.seed, self.batchsize, self.act_fn, self.nhidden, self.hindsight)

class CustomSimpleReplayPool(object):
    '''
    Edited to also contain next_ob as part of the data for convenience even though it is redundant.
    '''
    def __init__(
            self, max_pool_size, observation_dim, action_dim):
        self._observation_dim = observation_dim
        self._action_dim = action_dim
        self._max_pool_size = max_pool_size
        self._observations = np.zeros(
            (max_pool_size, observation_dim),
        )
        self._next_observations = np.zeros(
            (max_pool_size, observation_dim),
        )
        self._actions = np.zeros(
            (max_pool_size, action_dim),
        )
        self._rewards = np.zeros(max_pool_size)
        self._terminals = np.zeros(max_pool_size, dtype='uint8')
        self._bottom = 0
        self._top = 0
        self._size = 0
    
    def clear(self):
        '''
        Clear the pool of all samples.
        '''
        self._bottom = 0
        self._top = 0
        self._size = 0
        

    def add_sample(self, observation, action, next_observation, reward, terminal):
        self._observations[self._top] = observation
        self._next_observations[self._top] = next_observation
        self._actions[self._top] = action
        self._rewards[self._top] = reward
        self._terminals[self._top] = terminal
        self._top = (self._top + 1) % self._max_pool_size
        if self._size >= self._max_pool_size:
            self._bottom = (self._bottom + 1) % self._max_pool_size
        else:
            self._size += 1

    def random_batch(self, batch_size):
        assert self._size > batch_size
        indices = np.zeros(batch_size, dtype='uint64')
        count = 0
        while count < batch_size:
            index = np.random.randint(self._bottom, self._bottom + self._size) % self._max_pool_size
            # make sure that the transition is valid: if we are at the end of the pool, we need to discard
            # this sample
            if index == self._size - 1 and self._size <= self._max_pool_size:
                continue
            # if self._terminals[index]:
            #     continue
            indices[count] = index
            count += 1
        return dict(
            observations=self._observations[indices],
            actions=self._actions[indices],
            rewards=self._rewards[indices],
            terminals=self._terminals[indices],
            next_observations=self._next_observations[indices]
        )

    @property
    def size(self):
        return self._size


def custom_ddpg_train(algo, hindsight, senv):
    '''
    Customized version of DDPG train method.
    Additional bookeeping and simplifications.
    Updates and sampling are trajectory-based instead of step-based.
    Otherwise everything else is the same.
    '''
    # This seems like a rather sequential method
    pool = CustomSimpleReplayPool(
        max_pool_size=algo.replay_pool_size,
        observation_dim=algo.env.observation_space.flat_dim,
        action_dim=algo.env.action_space.flat_dim,
    )
    algo.start_worker()

    algo.init_opt()
    itr = 0
    terminal = False

    sample_policy = pickle.loads(pickle.dumps(algo.policy))
    
    train_stats = {'qlosses':[], 'esmean':[], 'esstd':[]}
    for epoch in range(algo.n_epochs):
        starttime = time.time()
        logger.push_prefix('epoch #%d | ' % epoch)
        logger.log("Training started")
        epoch_steps = 0
        bar = pyprind.ProgBar(algo.epoch_length)
        did_training = False
        while epoch_steps < algo.epoch_length:
            # Execute policy for one trajectory
            observations = [algo.env.reset()]
            actions = []
            rewards = []
            algo.es.reset()
            sample_policy.reset()
            path_length = 0
            path_return = 0
            terminal = False # indicates if episode ends with a terminal state
            for t in range(algo.max_path_length):
                action = algo.es.get_action(itr, observations[t], policy=sample_policy)  # qf=qf)
                next_observation, reward, terminal, _ = algo.env.step(action)
                path_length += 1
                path_return += reward
                observations.append(next_observation)
                actions.append(action)
                rewards.append(reward)
                if not terminal and path_length >= algo.max_path_length and algo.include_horizon_terminal_transitions:
                    # only set the last sample to be terminal if we want to treat final states as terminal
                    terminal = True
                # now add trajectory to the replay buffer
                # note that this will automatically add the actual goals under hindsight because we'd use an augmented env
                pool.add_sample(observations[t], actions[t], observations[t+1], rewards[t] * algo.scale_reward, terminal)
                # do at least one minibatch update
                if pool.size >= algo.min_pool_size:
                    for update_itr in range(algo.n_updates_per_sample):
                        #print('minibatch update')
                        # Train policy
                        batch = pool.random_batch(algo.batch_size)
                        algo.do_training(itr, batch)
                        did_training = True
                    sample_policy.set_param_values(algo.policy.get_param_values())
                itr += 1
                if epoch_steps+path_length <= algo.epoch_length:
                    bar.update()
                # if terminal, break out and start new trajectory
                if terminal:
                    break
                    
            epoch_steps += path_length
            algo.es_path_returns.append(path_return)
                
            # now for hindsight
            # and add more stuff to the replay buffer
            if hindsight:
                ngoals = len(senv.goals) # needed for computing offsets
                for t in range(path_length):
                    curr_cleaned_ob = observations[t][ngoals:]
                    next_cleaned_ob = observations[t+1][ngoals:]
                    #print('cleaned ob shape {}'.format(curr_cleaned_ob.shape))
                    #print('cleaned ob'.format(curr_cleaned_ob))
                    # do final or future
                    if False:
                        # do final
                        finalvals = np.array(observations[-1][ngoals:][senv.goalixs])
                        augmented_obs = np.concatenate([finalvals, curr_cleaned_ob]) # augment goal at the beginning
                        reward = 1.0 if np.all(np.abs(next_cleaned_ob[senv.goalixs] - finalvals) <= senv.goalthresholds) else 0.0
                        curr_terminal = reward > 0.99 # all rewarding states are terminal states
                        augmented_next_obs = np.concatenate([finalvals, next_cleaned_ob]) # augment with same goal at the beginning
                        pool.add_sample(augmented_obs, actions[t], augmented_next_obs, reward * algo.scale_reward, curr_terminal)
                    elif False:
                        # what if we artificially create the goals which we know interpolate between the current goals and the final goal?
                        # split the current goal difference from true goal into 5 parts, and pick one randomly to use as a goal
                        goalvals = senv.goalvals
                        currvals = observations[t][:ngoals]
                        randomfrac = np.random.random() * 5.0
                        interpolatedvals = currvals + ((goalvals - currvals) * randomfrac)
                        augmented_obs = np.concatenate([interpolatedvals, curr_cleaned_ob]) # augment goal at the beginning
                        reward = 1.0 if np.all(np.abs(next_cleaned_ob[senv.goalixs] - interpolatedvals) <= senv.goalthresholds) else 0.0
                        curr_terminal = reward > 0.99 # all rewarding states are terminal states
                        augmented_next_obs = np.concatenate([interpolatedvals, next_cleaned_ob]) # augment with same goal at the beginning
                        pool.add_sample(augmented_obs, actions[t], augmented_next_obs, reward * algo.scale_reward, curr_terminal)
                    else:
                        # 8 samples for future
                        for _ in range(8):
                            future_ix = np.random.randint(t, len(actions))
                            random_future_ob = observations[future_ix]
                            cleaned_future_ob = random_future_ob[ngoals:] # remove the augmented to get back to original
                            futurevals = np.array(cleaned_future_ob[senv.goalixs])
                            augmented_obs = np.concatenate([futurevals, curr_cleaned_ob]) # augment goal at the beginning
                            reward = 1.0 if np.all(np.abs(next_cleaned_ob[senv.goalixs] - futurevals) <= senv.goalthresholds) else 0.0
                            curr_terminal = reward > 0.99 # all rewarding states are terminal states
                            augmented_next_obs = np.concatenate([futurevals, next_cleaned_ob]) # augment with same goal at the beginning
                            #print('augmented obs {}'.format(augmented_obs))
                            #print('action {}'.format(action))
                            #print('augmented next obs {}'.format(augmented_next_obs))
                            #print('reward {}'.format(reward))
                            #print('terminal {}'.format(curr_terminal))
                            pool.add_sample(augmented_obs, actions[t], augmented_next_obs, reward * algo.scale_reward, curr_terminal)

        logger.log("Training finished")
        train_stats['esmean'].append(np.mean(algo.es_path_returns))
        train_stats['esstd'].append(np.std(algo.es_path_returns))
        # keep track of the qlosses
        if did_training:
            train_stats['qlosses'].append(np.mean(algo.qf_loss_averages))
        else:
            train_stats['qlosses'].append(-1.0)
        if did_training:
            algo.evaluate(epoch, pool)
            params = algo.get_epoch_snapshot(epoch)
            logger.save_itr_params(epoch, params)
        logger.dump_tabular(with_prefix=False)
        # end of epoch stats
        logger.log("Replay pool size {}".format(pool.size))
        logger.log("Steps Taken {}".format(epoch_steps))
        logger.log("Elapsed {:.3f}".format(time.time()-starttime))
        logger.pop_prefix()
        
    algo.env.terminate()
    algo.policy.terminate()
    return train_stats

def train_policy(root_prefix, trainpolparams):
    # note that the learned policy assumes a normalized environment
    
    set_seed(trainpolparams.seed)
    curpath = tp.get_policy_path(root_prefix, trainpolparams)
    logger.set_snapshot_dir(curpath)
    logger.set_snapshot_gap(1)
    logger.set_snapshot_mode('gap')

    env = normalize(trainpolparams.env)
    env.reset() # loads dynamic wrapper if hasn't been loaded before
    if trainpolparams.act_fn == 'tanh':
        policy_act_fn = NL.tanh
    elif trainpolparams.act_fn == 'relu':
        policy_act_fn = NL.rectify
    # seems like policy (32, 32) is probably fine
    policy = DeterministicMLPPolicy(
        env_spec=env.spec,
        hidden_nonlinearity=policy_act_fn,
        hidden_sizes=(trainpolparams.nhidden, trainpolparams.nhidden)
    )
    es = OUStrategy(env_spec=env.spec)
    qf = ContinuousMLPQFunction(env_spec=env.spec, hidden_sizes=(32, 32),)
    
    # many of the parameters below are pulled from the cartpole example on rllab
    # try to match each epoch with each iteration of trpo in terms of time, which is approximately 4 seconds
    trpo_batch_size = trainpolparams.horizon*trainpolparams.batchsize 
    mini_batch_size = 128 # default 32
    epoch_length = trpo_batch_size
    print('ddpg minibatch size {}'.format(mini_batch_size))
    print('ddpg epoch steps {}'.format(epoch_length))
    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=mini_batch_size,
        max_path_length=trainpolparams.horizon,
        epoch_length=epoch_length,
        min_pool_size=trpo_batch_size+1, # +1 so the first epoch is purely data gathering
        n_epochs=trainpolparams.num_iter,
        #include_horizon_terminal_transitions=True, # might be needed to stabilize the critic, otherwise it'll blow up
        discount=0.99,
        #scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
        eval_samples=100*trainpolparams.horizon, # might need more in case of stochastic ensemble
        #force_batch_sampler=True, # don't use vectorized sampler
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    train_stats = custom_ddpg_train(algo, trainpolparams.hindsight, trainpolparams.env)
    with open(os.path.join(curpath, 'train_stats.pkl'), 'wb') as f:
        pickle.dump(train_stats, f)

def train_policy_batch(root_prefix, trainpolparams, trajs):
    '''
    Custom training function for DDPG+Hindsight on a batch of data.
    Should be passed the oracle env to get the env_spec
    '''
    # note that the learned policy assumes a normalized environment
    assert(trainpolparams.hindsight) # make sure hindsight is being used for batch
    set_seed(trainpolparams.seed)
    curpath = tp.get_policy_path(root_prefix, trainpolparams)
    logger.set_snapshot_dir(curpath)
    logger.set_snapshot_gap(1)
    logger.set_snapshot_mode('gap')
    
    # remember the goal and reward threshold
    goalvals = trainpolparams.env.goalvals
    goalthresholds = trainpolparams.env.goalthresholds
    goalixs = trainpolparams.env.goalixs
    
    env = normalize(trainpolparams.env)
    env.reset() # loads dynamic wrapper if hasn't been loaded before
    if trainpolparams.act_fn == 'tanh':
        policy_act_fn = NL.tanh
    elif trainpolparams.act_fn == 'relu':
        policy_act_fn = NL.rectify
    # seems like policy (32, 32) is probably fine
    policy = DeterministicMLPPolicy(
        env_spec=env.spec,
        hidden_nonlinearity=policy_act_fn,
        hidden_sizes=(trainpolparams.nhidden, trainpolparams.nhidden)
    )
    es = OUStrategy(env_spec=env.spec)
    # looks like the qfunction size is really important
    # 500,500 seems to work well for swimmer
    # 1000,1000 seems to work well for halfcheetah
    qf = ContinuousMLPQFunction(env_spec=env.spec, hidden_sizes=(32, 32),)
    
    # how much tuples of data we have in total
    # multiplied by 9 because we generate 8 new goals
    data_horizon = len(trajs[0][1])
    ntuples = len(trajs) * data_horizon * 9
    batch_size = data_horizon*trainpolparams.batchsize
    epoch_length = int(ntuples / batch_size)
    print('ntuples {}'.format(ntuples))
    print('ddpg minibatch size {}'.format(batch_size))
    print('ddpg num batches {}'.format(epoch_length))
    
    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=batch_size,
        max_path_length=trainpolparams.horizon,
        epoch_length=epoch_length,
        min_pool_size=10000,
        n_epochs=trainpolparams.num_iter,
        discount=0.99, # use 0.95 maybe for hopefully more stable q-learning in batch
        #scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
        eval_samples=2*trainpolparams.horizon, # don't need that many samples to evaluate deterministic policy in determinstic env
        #force_batch_sampler=True, # don't use vectorized sampler
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    
    # don't call algo.train, instead implement custom train
    # code initially copied from algo.train method
    # This seems like a rather sequential method
    pool = CustomSimpleReplayPool(
        max_pool_size=algo.replay_pool_size,
        observation_dim=algo.env.observation_space.flat_dim, # assume env is goal augmented
        action_dim=algo.env.action_space.flat_dim,
    )
    
    algo.start_worker()

    algo.init_opt()
    itr = 0
    # keep track of the average q loss per epoch as a sign of convergence?
    qlosses = []
    for epoch in range(algo.n_epochs):
        starttime = time.time()
        logger.push_prefix('epoch #%d | ' % epoch)
        logger.log("Training started")
        
        # either update every epoch or just once at the beginning
        if False or epoch == 0:
            logger.log("Generating replay buffer with goals")
            # generate new goals and entire new replay buffer every epoch
            pool.clear()
            # populate the pool fully with the goal augmented data
            # also must terminate trajectories properly
            for i in range(len(trajs)):
                obs, acts, _ = trajs[i]
                # first add with original goal
                for t in range(len(acts)):
                    augmented_obs = np.concatenate([goalvals, obs[t]]) # augment goal at the beginning
                    next_obs = obs[t+1]
                    augmented_next_obs = np.concatenate([goalvals, next_obs]) # augment goal at the beginning
                    reward = 1.0 if np.all(np.abs(next_obs[goalixs] - goalvals) <= goalthresholds) else 0.0
                    terminal = reward > 0.99
                    pool.add_sample(augmented_obs, acts[t], augmented_next_obs, reward * algo.scale_reward, terminal)
                # next add future i.e. sample 8 goals from future steps in the trajectory
                for _ in range(8):
                    for t in range(len(acts)):
                        futurevals = np.array(obs[np.random.randint(t,len(acts))][goalixs])
                        augmented_obs = np.concatenate([futurevals, obs[t]]) # augment goal at the beginning
                        next_obs = obs[t+1]
                        augmented_next_obs = np.concatenate([futurevals, next_obs]) # augment goal at the beginning
                        reward = 1.0 if np.all(np.abs(next_obs[goalixs] - futurevals) <= goalthresholds) else 0.0
                        terminal = reward > 0.99
                        pool.add_sample(augmented_obs, acts[t], augmented_next_obs, reward * algo.scale_reward, terminal)
        
        logger.log("Starting mini-batch updates")
        for epoch_itr in range(algo.epoch_length):
            for update_itr in range(algo.n_updates_per_sample):
                # Train policy
                batch = pool.random_batch(algo.batch_size)
                algo.do_training(itr, batch)

            itr += 1

        logger.log("Training finished after {:.2f}s".format(time.time() - starttime))
        qlosses.append(np.mean(algo.qf_loss_averages))
        algo.evaluate(epoch, pool) # save time by not evaluating since not much to evaluate for batch setting
        
        # save policy snapshot
        params = algo.get_epoch_snapshot(epoch)
        logger.save_itr_params(epoch, params)
        logger.dump_tabular(with_prefix=False)
        logger.pop_prefix()
    # also save the qlosses
    with open(os.path.join(curpath, 'qlosses.pkl'), 'wb') as f:
        pickle.dump(qlosses, f)
        
    algo.env.terminate()
    algo.policy.terminate()