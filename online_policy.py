from rllab.policies.base import Policy
from sandbox.rocky.tf.envs.base import TfEnv
from rllab.envs.normalized_env import normalize

import train_policies as tp

import os
import joblib

import numpy as np
import tensorflow as tf
from collections import Counter

class PolicyEnsemble(Policy):

    def __init__(self, root_prefix, trainpolparams_list, policy_itr, error_decay, repeat_last):
        self.n = len(trainpolparams_list)
        self.errors = np.zeros(self.n)
        self.trainpolparams_list = trainpolparams_list
        self.policy_model_list = []
        self.graph_list = []
        self.session_list = []
        self.error_decay = error_decay
        self.repeat_last = repeat_last
        self.model_history = []
        for trainpolparams in trainpolparams_list:
            tfgraph = tf.Graph()
            sess = tf.Session(graph=tfgraph)
            with tfgraph.as_default(), sess.as_default():
                policy_path = tp.get_policy_path(root_prefix, trainpolparams)
                policy = joblib.load(os.path.join(policy_path, 'itr_{}.pkl'.format(policy_itr)))['policy']
                model = trainpolparams.env
                self.policy_model_list.append((policy, model))
                self.graph_list.append(tfgraph)
                self.session_list.append(sess)
        
    def get_action(self, observation):
        if len(self.model_history) > 0 and np.random.uniform() < self.repeat_last:
            model_n = self.model_history[-1]
        else:
            exp_errors = np.exp(-self.errors)
            probs = [x/sum(exp_errors) for x in exp_errors]
            model_n = np.random.choice(self.n, p = probs)
        self.model_history.append(model_n)
        with self.graph_list[model_n].as_default(), self.session_list[model_n].as_default():
            policy = self.policy_model_list[model_n][0]
            return policy.get_action(observation)

    def update_errors(self, state, action, nextobs):
        for i in range(self.n):
            with self.graph_list[i].as_default(), self.session_list[i].as_default():
                (policy, model) = self.policy_model_list[i]
                predicted = model.predict(state, action)
                self.errors[i] = np.mean((nextobs - predicted)**2) + (1.0 - self.error_decay)*self.errors[i]

    def most_common_model_freq(self):
        return int(Counter(self.model_history).most_common(1)[0][1]*10.0/len(self.model_history))

    def reset(self):
        for i in range(self.n):
            with self.graph_list[i].as_default(), self.session_list[i].as_default():
                (policy, model) = self.policy_model_list[i]
                policy.reset()
                model.reset()
        self.errors = np.zeros(self.n)
        self.model_history = []

def test_policy(root_prefix, trainpolparams_list, env, policy_itr, ntrials, error_decay = 0, repeat_last = 0):
    '''
    Given a Policy (rllab policy), and environment, test the policy and return stats.
    '''
    DEBUG = False
    tfgraph = tf.Graph()
    with tf.Session(graph=tfgraph) as sess:
        env = TfEnv(normalize(env))
        pol = PolicyEnsemble(root_prefix, trainpolparams_list, policy_itr, error_decay, repeat_last)
        rewards = np.zeros((ntrials,)) # only care about total sums of rewards
        most_common_model_freqs = np.zeros((ntrials,))
    for i in range(ntrials):
            ob = env.reset()
            pol.reset()
            if DEBUG:
                print('trial {}:'.format(i))
            for t in range(trainpolparams_list[0].horizon):
                a = pol.get_action(ob)[0]
                new_ob, r, done, _ = env.step(a)
                pol.update_errors(ob, a, new_ob)
                ob = new_ob
                rewards[i] += r
                if DEBUG:
                    #print('  s[-1]: {:.4f}'.format(ob[-1]))
                    print('  s: [{}]'.format(np2str(ob)))
                if done:
                    break
            most_common_model_freqs[i] = pol.most_common_model_freq()
    return rewards, most_common_model_freqs
