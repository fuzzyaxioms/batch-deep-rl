'''Generate batch data of various environments.'''

from rllab.envs.base import Env # basic env
from rllab.policies.base import Policy # basic policy
from rllab.envs.normalized_env import normalize

from custom_swimmer_env import CustomSwimmerEnv

import os
import time
import pickle
import joblib

import numpy as np

datapath = 'data'

def validate_path(path):
    '''Create folder if it doesn't already exist.'''
    if not os.path.exists(path):
        os.makedirs(path)

def rollout(env, policy, max_horizon):
    '''Collects trajectory information for a single rollout.'''
    DEBUG = False
    observations = []
    actions = []
    rewards = []
    agent_infos = []
    env_infos = []
    
    obs = env.reset()
    policy.reset()
    
    observations.append(env.observation_space.flatten(obs))
    
    for i in range(max_horizon):
        a, agent_info = policy.get_action(obs)
        next_obs, r, done, env_info = env.step(a)
        observations.append(env.observation_space.flatten(next_obs))
        rewards.append(r)
        actions.append(env.action_space.flatten(a))
        agent_infos.append(agent_info)
        env_infos.append(env_info)
        if DEBUG:
            print('ob {} a {} -> next ob {} r {}'.format(obs, a, next_obs, r))
        if done:
            break

    return dict(
        observations=observations,
        actions=actions,
        rewards=rewards,
        agent_infos=agent_infos,
        env_infos=env_infos,
    )

class RandomPolicy(Policy):
    '''Uniformly random policy.'''
    def __init__(self, env_spec):
        super(RandomPolicy, self).__init__(env_spec)
    
    def get_action(self, ob):
        '''Returns (action, info).'''
        return (self.action_space.sample(), None)

class DataParams(object):
    def __init__(self, env, horizon, ntraj, filter_reward, policypath=None, policyname=None):
        self.env = env
        self.horizon = horizon
        self.ntraj = ntraj
        self.filter_reward = filter_reward
        self.policypath = policypath
        self.policyname = policyname
    
    def name(self):
        policystr = '-{}'.format(self.policyname) if self.policyname is not None else ''
        filterstr = '-filtered' if self.filter_reward else ''
        return '{}-h{}-n{}{}{}'.format(self.env.name(), self.horizon, self.ntraj, filterstr, policystr)
    
    def filename(self, root_prefix):
        global datapath
        return os.path.join(root_prefix, datapath, self.name()+'.pkl') 

def generate_data(root_prefix, dataparams):
    global datapath
    
    print('Generating data')
    validate_path(os.path.join(root_prefix,datapath))
    savefilepath = dataparams.filename(root_prefix)
    # don't generate if already exists
    if os.path.exists(savefilepath):
        print('Already generated')
        return
    
    env = dataparams.env
    if dataparams.policypath is None:
        policy = RandomPolicy(env.spec)
    else:
        policy = joblib.load(dataparams.policypath)['policy']
    
    num_duds = 0
    trajs = []
    while len(trajs) < dataparams.ntraj:
        tdata = rollout(env, policy, dataparams.horizon)
        # filter out rewarding trajectories if toggled
        if dataparams.filter_reward:
            # check whether there were rewards
            rewarding = np.array(tdata['rewards']) > 0.5
            if not np.any(rewarding):
                trajs.append((tdata['observations'],tdata['actions'],tdata['rewards']))
            else:
                num_duds += 1
        else:
            trajs.append((tdata['observations'],tdata['actions'],tdata['rewards']))
    print('Num thrown out {}/{} = {:.4f}'.format(num_duds, num_duds+dataparams.ntraj, 1.0*num_duds/(num_duds+dataparams.ntraj)))
    with open(savefilepath,'wb') as f:
        pickle.dump(trajs, f)
        
    return

def load_data(root_prefix, dataparams):
    savefilepath = dataparams.filename(root_prefix)
    with open(savefilepath,'rb') as f:
        trajs = pickle.load(f)
        return trajs
    

def debug():
    starttime = time.time()
    
    goalvel = 0.08
    root_prefix = '.'
    
    senv = CustomSwimmerEnv(goalvel)
    
    dparams = DataParams(env=senv, horizon=20, ntraj=1)
    generate_data(root_prefix, dparams)
    trajs = load_data(root_prefix, dparams)
    print(trajs)
    
    elapsed = time.time() - starttime
    print('Elapsed {:.3f}s'.format(elapsed))

if __name__ == '__main__':
    debug()
