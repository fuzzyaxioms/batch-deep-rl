import matplotlib
matplotlib.use('agg')

import matplotlib.pyplot as plt
import numpy as np
import pickle


def plot_results(file_dict, plot_name):
    datad = {}
    for x in file_dict:
        datad[x] = pickle.load(open(file_dict[x], 'rb'))

    plotd = {}
    for x in datad:
        a = []
        for b in datad[x]:
            if isinstance(datad[x][b], dict):
                a += datad[x][b].values()
            else:
                a.append(datad[x][b])
        a = [np.mean(b, axis = 0)[[True, False, True]] for b in zip(*a)]
        plotd[x] = list(zip(*a))


    for x in plotd:
        plt.plot(plotd[x][0], plotd[x][1], label = x)

    plt.legend()
    plt.savefig(plot_name)
    plt.clf()



def plot_mn():
    filename = "results/online_results_mn{}_ed0_rl0.pkl"
    filename_old = "results/online_results_mn{}.pkl"
    filed = {}
    filed["1 model"] = "results.pkl"
    filed["10 models"] = filename.format(10)
    filed["25 models"] = filename_old.format(25)
    filed["50 models"] = filename_old.format(50) 

    plot_results(filed, "plots/mean_reward_mn_ed0_rl0.png")

def plot_rl():
    filename = "results/online_results_mn1_ed0_rl{}.pkl"
    filed = {}
    filed["0 repeat"] = filename.format(0)
    filed["0.2 repeat"] = filename.format(0.2)
    filed["0.5 repeat"] = filename.format(0.5)
    filed["0.8 repeat"] = filename.format(0.8)

    plot_results(filed, "plots/mean_reward_mn1_ed0_rl.png")

def plot_ed():
    filename = "results/online_results_mn1_ed{}_rl0.pkl"
    filed = {}
    filed["0 decay"] = filename.format(0)
    filed["0.2 decay"] = filename.format(0.2)
    filed["0.5 decay"] = filename.format(0.5)
    filed["0.8 decay"] = filename.format(0.8)
    filed["1 decay"] = filename.format(1)


    plot_results(filed, "plots/mean_reward_mn1_ed_rl0.png")

def plot_freq_results(file_dict, plot_name):
    datad = {}
    for x in file_dict:
        datad[x] = pickle.load(open(file_dict[x], 'rb'))

    plotd = {}
    for x in datad:
        plotd[x] = list()
        for a in datad[x]:
            plotd[x].append((a*0.1, np.mean(datad[x][a])))
        plotd[x] = list(zip(*plotd[x]))
    for x in plotd:
        plt.plot(plotd[x][0], plotd[x][1], label = x)
    print(plotd.keys())
    plt.legend()
    plt.savefig(plot_name)
    plt.clf()

def plot_freq_hist(file_dict, plot_name):
    datad = {}
    for x in file_dict:
        datad[x] = pickle.load(open(file_dict[x], 'rb'))

    plotd = {}
    for x in datad:
        plotd[x] = list()
        for a in datad[x]:
            plotd[x].append((a*0.1, len(datad[x][a])))
        plotd[x] = list(zip(*plotd[x]))
    for x in plotd:
        plt.plot(plotd[x][0], plotd[x][1], label = x)
    print(plotd)
    plt.legend()
    plt.savefig(plot_name)
    plt.clf()



def plot_freqr():
    filename = "results/online_freq_results_mn{}_ed{}_rl{}.pkl"
    filed = {}
#    filed["10mn, 1 ed, 0.8 rl"] = filename.format(10, 1, 0.8)
    filed["10mn, 0.8 ed, 0.8 rl"] = filename.format(10, 0.8, 0.8)
    filed["50mn, 0.8 ed, 0.8 rl"] = filename.format(50, 0.8, 0.8)
    
    plot_freq_results(filed, "plots/freq_reward_mn10_ed0.8_rl0.8.png")

def plot_freqh():
    filename = "results/online_freq_results_mn{}_ed{}_rl{}.pkl"
    filed = {}
#    filed["10mn, 1 ed, 0.8 rl"] = filename.format(10, 1, 0.8)
    filed["10mn, 0.8 ed, 0.8 rl"] = filename.format(10, 0.8, 0.8)
    
    plot_freq_hist(filed, "plots/freq_hist_mn10_ed0.8_rl0.8.png")


def main():
#    plot_mn()
#    plot_rl()
#    plot_ed()
    plot_freqr()
    plot_freqh()
if __name__ == "__main__":
    main()
